import { HIDE_TAB_BAR } from "../actionType";

const initialState = {
    hide: false
}


export const tabBarVisible = (state = initialState, action) => {
    switch (action.type) {
        case HIDE_TAB_BAR:
            return Object.assign({}, state, {
                hide: action.data
            })
        default:
            return state
    }


}