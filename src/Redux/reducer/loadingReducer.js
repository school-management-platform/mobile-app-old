import { SET_LOADING } from "../actionType";

const initialState = {
    loading: false,
    succcess: false
}


export const loadingVisible = (state = initialState, action) => {
    switch (action.type) {
        case SET_LOADING:
            return Object.assign({}, state, {
                loading: action.data
            })
        default:
            return state
    }


}