import { HIDE_NAV, SHOW_TIME_IN_MODAL, TIME_IN, TIME_OUT } from "../actionType";

const initialState = {
    hide: false,
    in: null,
    out: null,
    hideNav: false
}


export const timeInVisible = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_TIME_IN_MODAL:
            return Object.assign({}, state, {
                hide: action.data
            })
        case TIME_IN:
            return Object.assign({}, state, {
                in: action.data
            })
        case TIME_OUT:
            return Object.assign({}, state, {
                out: action.data
            })
        case HIDE_NAV:
            return Object.assign({}, state, {
                hideNav: action.data
            })
            
        default:
            return state
    }


}