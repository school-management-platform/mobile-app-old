import { LOG_IN, PRE_SELECT, REMEMBER, SCHOOL_LOGIN, SET_DISTRICT, SET_LOGOUT, SET_SEARCH_DRAWER, SET_SUBJECT, SET_USER, SHOW_CHANGE, UPDATE_USER } from "../actionType";

const initialState = {
    details: {},
    selectedSubject: '',
    toBeSelect: '',
    showChangeSubj: false,
     email: '',
        password: '',
    school_password: '',
    remember: false,
    searchItemDrawer: '',
    tempItemDrawer: [],
    district: 0
}

    // email: 'franzloren10@gmail.com',
    //     password: '20080610'
    // school_password: 'bCc*SiNCe1947'
    // email: 'johnadoe@gmail.com',
    //     password: 'password'

// bCc * SiNCe1947


export const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER:
            
            return Object.assign({}, state, {
                details: action.data,
                tempItemDrawer: action.data.classes,
                selectedSubject: action.data.classes.length != 0 ? action.data.classes[0] : null,
                toBeSelect: action.data.classes.length != 0 ? action.data.classes[0] : null
            })
        case UPDATE_USER:

            return Object.assign({}, state, {
                details: action.data,
            })

        case SET_DISTRICT:

            return Object.assign({}, state, {
                district: action.data,
            })
        case SET_LOGOUT:
            
            return Object.assign({}, state, {
                details: {},
                tempItemDrawer: {},
                selectedSubject: null,
                email: '',
                password: '',
                school_password: '',
                toBeSelect:  null,
                remember: false
            })
        case SET_SUBJECT:
            return Object.assign({}, state, {
                selectedSubject: action.data
            })
        case PRE_SELECT:
            return Object.assign({}, state, {
                toBeSelect: action.data
            })
        case SHOW_CHANGE:
            
            return Object.assign({}, state, {
                showChangeSubj: action.data
            })

        case LOG_IN: 
            return Object.assign({}, state, {
                email: action.email,
                password: action.password
            })

        case SCHOOL_LOGIN :

            return Object.assign({}, state, {
                school_password: action.data
            })

        case REMEMBER: 
            
            return Object.assign({}, state, {
                remember: action.data
            })

        case SET_SEARCH_DRAWER:
            
            return Object.assign({}, state, {
                searchItemDrawer: action.search,
                tempItemDrawer: action.data
            })

        default:
            return state
    }
}