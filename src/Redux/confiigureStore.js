import { applyMiddleware, compose, createStore, combineReducers } from 'redux'
import { persistStore, persistReducer } from 'redux-persist';

import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';
import autoMergeLevel2 from 'redux-persist/es/stateReconciler/autoMergeLevel2';
import { tabBarVisible } from "./reducer/tabBarReducer";
import { timeInVisible } from './reducer/TimeInReducer';
import {loadingVisible} from './reducer/loadingReducer'
import {UserReducer} from './reducer/UserReducer'

const combine = combineReducers({
    tabBarVisible: tabBarVisible,
    timeInVisible: timeInVisible,
    loadingVisible: loadingVisible,
    user: UserReducer,

})




const persistConfig = {
    key: 'elsa',
    storage: AsyncStorage,
    version: 0,
    stateReconciler: autoMergeLevel2, // see "Merge Process" section for details.
};

const persistedReducer = persistReducer(persistConfig, combine);
export const store = createStore(
    persistedReducer,
    compose(applyMiddleware(thunk)),
);
export const persistor = persistStore(store);