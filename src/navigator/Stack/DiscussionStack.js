import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';

import { Discussion } from '../../views/Discussioon/Discussion';
import { Logs } from '../../views/Titos/Logs';

const Stack = createStackNavigator();

export const DiscussionStack = (props) => {

    return (
        <Stack.Navigator
            screenOptions={{ headerShown: false, gestureEnabled: false }}>
            <Stack.Screen name={'Discussion'} component={Discussion} />
            <Stack.Screen name={'Logs'} component={Logs} />
        </Stack.Navigator>
    )
}