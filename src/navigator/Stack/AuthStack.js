import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Landing from '../../views/Landing/Landing';
import { DrawerNav } from '../Drawer/DrawerStack';
import { LoadingPage } from '../../views/LoadingPage';
import { useSelector } from 'react-redux';

const Stack = createStackNavigator();

export const AuthStack = (props) => {

    const relog = useSelector(state => state.user.remember)

    
    
    return(
        <Stack.Navigator 
        initialRouteName={ "Auth"}
        screenOptions={{headerShown: false, gestureEnabled: true}}>
            <Stack.Screen  name={'Auth'} component={Landing}/>
            <Stack.Screen name={'Loading'} component={LoadingPage} />
            <Stack.Screen options={{gestureEnabled: false}} name={'HomeDrawer'} component={DrawerNav} />
        </Stack.Navigator>
    )
}