import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { Home } from '../../views/Home/Home';
import { Announcement } from '../../views/Home/Announcement';
import { Logs } from '../../views/Titos/Logs';

const Stack = createStackNavigator();

export const HomeStack = (props) => {

    return (
        <Stack.Navigator  screenOptions={{ headerShown: false, gestureEnabled:false }}>
            <Stack.Screen options={{gestureEnabled: false}} name={'Home'} component={Home} />
            <Stack.Screen name={'Announcement'} component={Announcement} />
            <Stack.Screen name={'Logs'} component={Logs} />
        </Stack.Navigator>
    )
}