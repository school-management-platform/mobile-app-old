import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { DocumentsFile } from '../../views/Upload/Documents';
import DocumentViewing from '../../views/Upload/DocumentViewing';
import { LinkViewing } from '../../views/Upload/LinkViewing';
import { Logs } from '../../views/Titos/Logs';


const Stack = createStackNavigator();

export const DocumentStack = (props) => {

    return (
        <Stack.Navigator screenOptions={{ headerShown: false, gestureEnabled: false }}>
            <Stack.Screen name={'Document'} component={DocumentsFile} />
            <Stack.Screen name={'Viewing'} component={DocumentViewing} />
            <Stack.Screen name={'Link'} component={LinkViewing} />
            <Stack.Screen name={'Logs'} component={Logs} />
            
        </Stack.Navigator>
    )
}