import  React, {useEffect, useState} from 'react';
import {
    createDrawerNavigator, DrawerContentScrollView,
    DrawerItemList, DrawerItem
} from '@react-navigation/drawer';
import { Text, View, SafeAreaView, ScrollView, TouchableWithoutFeedback, StyleSheet } from "react-native";
import { TabNav } from '../Tab/Tab';
import { DrawerHeader } from '../../components/Header';
import { AllSubject, SubjectItemDrawer } from '../../components/Subject';
import {SubjectConfirmationModal} from '../../components/Modal'
import {store} from '../../Redux/confiigureStore'
import { PRE_SELECT, SET_LOGOUT, SET_SEARCH_DRAWER, SET_SUBJECT, SET_USER, SHOW_CHANGE } from '../../Redux/actionType';

import {useSelector, useDispatch} from 'react-redux'
import { DeskSVG } from '../../components/IconSvg';
import { styles } from '../../stylesheet/style';
import { ApiCall } from '../../utils/api';
import { useNavigation } from '@react-navigation/native';
const Drawer = createDrawerNavigator();


const  CustomDrawerContent = (props) => {
    
    let stored = store.getState()
    // let dispatch = store.dispatch()
    let userData = stored.user.details
    let data = stored.user
    



    

    const searchClass = (v) => {

        let initial = userData.classes
        let result = initial.filter(item => {
            let name = item.name.toLowerCase()
          return name.includes(v.toLowerCase())
        })

        
        // setData(result)
    
        

        store.dispatch({
            type: SET_SEARCH_DRAWER,
            data: result,
            searchItemDrawer: v
        })
        // setSearch(v)
    }

    

    return (


        <View style={{ flex: 1}}>
            {/* <SubjectConfirmationModal 
                show={stored.user.showChangeSubj}
                subject={stored.user.toBeSelect}
            /> */}
            <ScrollView>
                <DrawerHeader 
                    onChangeText={(v) => {
                        searchClass(v)
                    }}
                     data={userData} />
                    <View style={{ paddingHorizontal: 20}}>
                    <SubjectItemDrawer
                        selectedSubject={2}
                        onPress={(item) => {
                            console.log(item, "--> RESULT")
                            store.dispatch({
                                type: PRE_SELECT,
                                data: item
                            })
                    
                               store.dispatch({
                                   type: SHOW_CHANGE,
                                   data: true
                               })
                 
                        
                        }}
                        item={data.tempItemDrawer}
                    />
                    </View>
            </ScrollView>
            <SafeAreaView style={{}}>
                {/* <AllSubject/> */}
                <TouchableWithoutFeedback onPress={() => {
                    ApiCall('/auth/logout', null, "post", stored.user.details.token).then(res => {
                     
                        props.navigation.navigate('Auth')
                        setTimeout(() => {
                            store.dispatch({
                                type: SET_LOGOUT,

                            })
                               
                        }, 1500)
                       
                    })
                 
                }}>
                    <View style={[style.allSubject, style.alternateShadow]}>
                        {/* <DeskSVG height={24} width={24} fill="#fff" /> */}
                        <Text style={[styles['text-label'], { color: "#fff", fontWeight: "600", lineHeight: 19, marginLeft: 20 }]}>LOGOUT</Text>
                    </View>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        </View>


    );
}

const style = StyleSheet.create({

    alternateShadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: -5,
        },
        shadowOpacity: 0.3,
        shadowRadius: 10,

        elevation: 5,
    },
    allSubject: {
        backgroundColor: styles['color-blue'].backgroundColor,
        width: "100%",
        height: 60,
        alignItems: "center",
        paddingHorizontal: 20,
        flexDirection: 'row'
    },

})





export const DrawerNav = (props) => {
    const state = useSelector(state => state.user)
    
    const dispatch = useDispatch()
    useEffect(() => {
        return(() => {

        })
    }, [])
    return (
        <>
            <SubjectConfirmationModal
                onChange={() => {
                    dispatch({
                        type: SHOW_CHANGE,
                        data: false
                    })
                    dispatch({
                        type: SET_SUBJECT,
                        data: state.toBeSelect
                    })

                    props.navigation.navigate('Home')
                }}
                cancel={ () => {
                    dispatch({
                        type: SHOW_CHANGE,
                        data: false
                    })
                }}
                show={state.showChangeSubj}
                subject={state.toBeSelect}
            />
        <Drawer.Navigator
            drawerContent={CustomDrawerContent}
            drawerType={"slide"} drawerStyle={{
                width: "85%"
            }}
                screenOptions={{
                    swipeEnabled: true,
                    gestureEnabled: true
                }}
            >
           
            <Drawer.Screen  options={{
                drawerLabel: () => null,
            }} name="Home" component={TabNav} />
        </Drawer.Navigator>
        </>
    )

}