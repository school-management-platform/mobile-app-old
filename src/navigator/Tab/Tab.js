import React, {useState, useEffect} from 'react'
import { View, TouchableWithoutFeedback, StyleSheet, Text, Platform } from "react-native";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'

import { Home } from '../../views/Home/Home';
import { styles } from "../../stylesheet/style";
import { HomeSVG, VirtualSVG, UploadSVG, ChatSVG, TimeSVG } from "../../components/IconSvg";
import { HomeStack } from '../Stack/HomeStack';
import { useSelector, useDispatch } from "react-redux";
import  {DocumentStack} from '../Stack/FileManagerStack'
import Links from '../../views/Links/Links';
import { getFocusedRouteNameFromRoute, Link, useRoute } from '@react-navigation/native';
import { TimeInModal, ConfirmTimeInModal, AlertModal } from '../../components/Modal';
import { Discussion } from '../../views/Discussioon/Discussion';
import { DiscussionStack } from '../Stack/DiscussionStack';
import { TIME_IN, TIME_OUT } from '../../Redux/actionType';
import { ApiCall } from '../../utils/api';

const Tab = createBottomTabNavigator();

export const TabNav = (props) => {
    const routeScreen = useRoute()
    const UserData = useSelector(state => state.user)
    const subject = useSelector(state => state.user.selectedSubject)
    const stateTime = useSelector(state => state.timeInVisible)
    const hideNav = useSelector(state => state.hideNav)
    const district = useSelector(state => state.user.district)
    const dispatch = useDispatch()
    const [show, setShow] = useState(false)
    const [confirmShow, setConfirmShow] = useState(false)
    const [time, setTime] = useState('')
    const [succ,setSuccess] = useState(false)


    

    const getTabBarVisibility = (route) => {


        const routeName = getFocusedRouteNameFromRoute(route);
        
        const hideOnScreens = ['Announcement', 'Viewing', 'Discussion', "Link", "Logs"];
        if (hideOnScreens.indexOf(routeName) > -1 || route.name == "Discussion"){
            return false
        }
        else{
            return true
        }

      
    };

    const titos = (val) => {
        let params = {
            class_id: UserData.selectedSubject.class_id,
            status: val
        }
        ApiCall('/student/attend', params, "post", UserData.details.token, district).then(res => {
            console.log(res, "--> HAHA")
            if (res.status == 200) {
               
            }
            else {
              
            }
        })
    }

    // let routeIndex = props.route.state.index

    return (

       <View style={{flex: 1, backgroundColor:"#fff"}}>
           <TimeInModal
                show={show}
                goLogs={() => {
                    setShow(false)
                    props.navigation.navigate('Logs')
                }}
                type="other"
                in={stateTime.in != null ? false : true}
                cancel={() => setShow(false)}
                confirm={(time) => {
                    setTime(time)
                    setShow(false)

                    

                  

                    setTimeout(() => {
                        setConfirmShow(true)
                    }, 1000)
                }}
                exit={() => setShow(false)}
           />
            <TimeInModal
                type="confirm"
                in={stateTime.in != null ? false : true}
                cancel={() => setConfirmShow(false)}
                show={confirmShow}
                confirm={() => {
                    setConfirmShow(false)

                    if (stateTime.in == null) {
                        
                        dispatch({
                            type: TIME_IN,
                            data: time
                        })

                        titos(1)
                    }
                    else if (stateTime.out == null && stateTime.in != null) {
                        
                        dispatch({
                            type: TIME_OUT,
                            data: time
                        })

                        dispatch({
                            type: TIME_IN,
                            data: null
                        })
                        titos(0)
                    }
                    else {
                        
                        dispatch({
                            type: TIME_OUT,
                            data: null
                        })

                        dispatch({
                            type: TIME_IN,
                            data: null
                        })
                        titos(0)
                    }

                    

                    setTimeout(() => {
                        setSuccess(true)
                        setTimeout(() => {
                            setSuccess(false)
                        }, 2000)
                    }, 1000)
                }}
                exit={() => setConfirmShow(false)}
            />
            <AlertModal
                in={stateTime.in != null ? true : false}
                show={succ}
                time={time}
            />

            <Tab.Navigator
                initialRouteName={"HomeTab"}
                tabBarOptions={{
                    
                    showLabel: false,
                    activeTintColor: "#fff",
                    inactiveTintColor: "#000",
                    style: {
                        height: 90,
                        // paddingVertical: 20,
                        overflow: "visible",
                        backgroundColor: "#fff",
                        position: "absolute",
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: -2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,

                        elevation: 5,
                        scrollEnabled: false

                    },
                    tabStyle: {
                        // backgroundColor: "#fff",
                        overflow: stateTime.hideNav  ? "hidden" : "visible"
                    }
                }}>

                   

                <Tab.Screen name="HomeTab"

                    options={({ route }) => ({
                        showLabel: false,
                        tabBarIcon: ({ color, size, backgroundColor, focused }) => (
                            <View style={[tabStyle.box, { backgroundColor: focused ? styles['color-mid-blue'].color : "transparent" }]}>
                                <HomeSVG width={24} height={24} fill={color} />
                            </View>
                        ),

                        tabBarVisible: getTabBarVisibility(route)
                    })}
                    component={HomeStack} />
                <Tab.Screen
                    options={{
                        tabBarLabel: 'Links',
                        tabBarIcon: ({ color, size, backgroundColor, focused }) => (
                            <View style={[tabStyle.box, { backgroundColor: focused ? styles['color-mid-blue'].color : "transparent" }]}>
                                <VirtualSVG width={24} height={24} fill={color} />
                            </View>
                        ),
                    }}
                    name="Links" component={Links} />

                <Tab.Screen
                    options={({route}) => ({
         
                        tabBarIcon: () => (
                           <TouchableWithoutFeedback onPress={() => setShow(true)}>
                                <View style={{ overflow: "visible", zIndex: 7, height: 68, width: 68, alignItems: "center", borderRadius: 68 / 2, backgroundColor: styles['color-blue'].backgroundColor, justifyContent: "center", bottom: Platform.OS == "ios" ? 30 : 40 }}>
                      
                                    <TimeSVG width={27} height={27} fill="#fff" />
                                </View>
                           </TouchableWithoutFeedback>
                        ),
                        tabBarVisible: getTabBarVisibility(route)
                    })}
                    name="Other" component={Links} />

                <Tab.Screen
                    options={({ route }) => ({
                        tabBarLabel: 'Upload',
                        tabBarIcon: ({ color, size, focused }) => (
                            <View style={[tabStyle.box, { backgroundColor: focused ? styles['color-mid-blue'].color : "transparent" }]}>
                                <UploadSVG width={24} height={24} fill={color} />
                            </View>
                        ),
                        tabBarVisible: getTabBarVisibility(route)
                    })}
                    name="Folder" component={DocumentStack} />
                <Tab.Screen
                    options={({route}) => ({
                        tabBarLabel: 'Chat',
                        tabBarIcon: ({ color, size, focused }) => (
                            <View style={[tabStyle.box, { backgroundColor: focused ? styles['color-mid-blue'].color : "transparent" }]}>
                                <ChatSVG width={24} height={24} fill={color} />
                            </View>
                        ),
                        tabBarVisible: getTabBarVisibility(route)
                    })}
                    name="Discussion" component={DiscussionStack} />
            </Tab.Navigator>
       </View>

    )
}

const tabStyle = StyleSheet.create({
    box: {
        height: 44,
        width: 44,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 8,
    }
})