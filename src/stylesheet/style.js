import React from 'react'
import { StyleSheet } from "react-native";


export const styles  = StyleSheet.create({
    Container:{
        flex: 1,
        backgroundColor:"#fff"
      
    },
    shadow:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    'color-neutral':{
        color:"#E7ECF3"
    },  
    "color-primary":{
        color:"#4A89E8",
        
    },
    "color-mid-blue":{
        color:"#7BC8FF"
    },  
    'color-blue':{
        backgroundColor:"#4A89E8"
    },
    'color-navy-blue':{
        color: "#212E5C"
    },
    'color-peach':{
        color: '#FFCECE'
    },
    'color-brown':{
        color:"#943E3E"
    },
    'color-dark-blue':{
        color: "#386CBB"
    }  ,
    'color-white':{
        color:"#fff",
        backgroundColor:"#fff"
    },
    'color-hover':{
        color: "#7BC8FF"
    },
    'color-background' : {
        backgroundColor:"#386CBB"
    },
    'color-grey-blue': {
        color:"#8E93A5"
    },
    "text-header":{
        fontSize: 16,
        fontWeight:"600",
        lineHeight: 21,
        fontFamily:"NunitoSans-Regular"
    },
    'text-paragpraph': {
        fontSize: 16,
        fontWeight:"400",
        lineHeight: 21,
        fontFamily: "NunitoSans-Regular"
    },
    'text-label': {
        fontWeight:"600",
        fontSize: 14,
        lineHeight: 19,
        fontFamily: "NunitoSans-Regular"
    },
    'text-label-field':{
        fontWeight: "600",
        fontSize: 16,
        lineHeight: 21,
        fontFamily: "NunitoSans-Regular"
    },
    'text-mute': {
        fontSize: "600",
        fontSize: 14,
        lineHeight: 21,
        fontFamily: "NunitoSans-Regular"
    },
    'text-table-data': {
        fontSize: "600",
        fontSize: 14,
        lineHeight: 19,
        fontFamily: "NunitoSans-Regular"
    },
    'text-input-style':{
        borderRadius: 8,
        paddingVertical: 18,
        paddingHorizontal: 18,
        backgroundColor:"#fff",
        fontFamily: "NunitoSans-Regular"
    },
    "icon-size": {
        height: 24,
        width: 24
    },
    'with-border':{
        borderWidth: 1,
        borderColor:"#CCD1D9"
    }

})