import moment from 'moment'
import React, {useState, useRef, useEffect} from 'react'
import { View, Text, SafeAreaView, Dimensions, FlatList, ScrollView, Animated, Platform } from 'react-native'
import { SubjectCard } from '../../components/Card'
import { Header } from '../../components/Header'
import { styles } from '../../stylesheet/style'
const {width} = Dimensions.get('screen')
var timer;
import DeviceInfo from 'react-native-device-info';
import { baseUrl, baseurl2 } from '../../utils/api'
import { useSelector } from 'react-redux'
import axios from 'axios'
const Links = (props) => {
    const UserData = useSelector(state => state.user)
    const district = useSelector(state => state.user.district)
    const subject = useSelector(state => state.user.selectedSubject)
    let hasNotch = DeviceInfo.hasNotch();
    let offset = 0
    let startOffset = 0
    const sticky = useRef(new Animated.Value(Platform.OS == "ios" && hasNotch ? 120 : 66))
    const stickyHeader = useRef(new Animated.Value(0))
    const opacity = useRef(new Animated.Value(1))
    
    const [meeting, setMeeting] = useState([])
    

    const toggleDrawer = () => {
        props.navigation.toggleDrawer()
    }

    useEffect(() => {
        getMeetings()
        return(() => {

        })
    }, [subject != null && subject.name])

    const getMeetings = () => {

        let settingParams = {
            class_id: UserData.selectedSubject.class_id,
            section_id: UserData.selectedSubject.section_id
        }

        axios.get((district == 1 ? baseUrl : baseurl2) + "/meeting/all", { params: settingParams, headers: { "AUTHORIZATION": `Bearer ${UserData.details.token}` } }).then(res => {
            

            setMeeting(res.data.data)
        }).catch(error => {
            setMeeting([])
        })
    }

   
    
    const getToday = (data) => {
        let today = moment(new Date()).format("MM/DD/YYYY")
        


        if(data == "today"){
         let a = meeting.filter(v => {
             let formmated = moment(v.updated_at).format('MM/DD/YYYY')
             return moment(formmated).isSame(today, "day")
            })

            return a
        
        }
        else if(data == "past"){
         
            let a = meeting.filter(v =>{
      
                let formmated = moment(v.updated_at).format('MM/DD/YYYY')
            
                
                
                return moment(formmated).isBefore(today, "day")
            })
            
            return a
        }
        else if (data == "upcoming") {

            let a = meeting.filter(v => {
                let formmated = moment(v.updated_at).format('MM/DD/YYYY')



                return moment(formmated).isAfter(today, "day")
            })

            return a
        }
    }
 

    const onScroll =async (event) => {
        
        
        // clearTimeout(timer)
        
        
        var currentOffset = event.nativeEvent.contentOffset.y;
        
        var direction = currentOffset > offset && currentOffset != 0 ? 'down' : 'up';
     

     
        
        if(direction == "down"){
            Animated.parallel([
                // if with notchh make it 30 if not 0
                Animated.timing(sticky.current, {
                    toValue: Platform.OS == "ios" && hasNotch ?  30 : 0,
                    duration: 70,
                    useNativeDriver: false
                }),
                Animated.timing(stickyHeader.current, {
                    toValue: -20,
                    duration: 70,
                    useNativeDriver: false
                }),
                  Animated.timing(opacity.current, {
                      toValue: 0,
                      duration: 70,
                      useNativeDriver: false
                  })
            ]).start()

         
         
           
        }
        else if(direction == "up"){
            Animated.parallel([
                Animated.timing(stickyHeader.current, {
                toValue: -10,
                duration: 70,
                useNativeDriver: false
            }),
            Animated.timing(sticky.current, {
                // 66 if no notch 110 if with notch
                toValue: Platform.OS == "ios" && hasNotch ?  110 : 66,
                duration: 70,
                useNativeDriver: false
            }),
                Animated.timing(opacity.current, {
                    toValue: 1,
                    duration: 70,
                    useNativeDriver: false
                })
        ]).start()
        }
        offset = currentOffset

     
    }

    const _onScrollBeginDrag = (event) => {
        //event.nativeEvent.contentOffset.y represents the offset of the Y-axis scroll
        const offsetY = event.nativeEvent.contentOffset.y;
       
        startOffset = offsetY;
    }

    return (
        <SafeAreaView style={[styles.Container]}>
            <View/>
            <Animated.View style={{ backgroundColor: "#fff", opacity: opacity.current, transform: [{ translateY: stickyHeader.current }]}}>
                <Header
                    onToggle={toggleDrawer}
                    containerStyle={{ position: 'relative', width: width }}
                />
            </Animated.View>
            <Animated.View style={{
                padding: 20, flexDirection: "row", justifyContent: "space-between", backgroundColor: "#fff", position:"absolute",
                zIndex: 99, width: width, transform: [{ translateY: sticky.current }]
            }}>
                <View>
                    <Text style={[styles['text-header'], { fontWeight: '700', fontSize: 20 }]}>Virtual Links</Text>
                    <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>{subject.name}</Text>
                </View>

                <View style={{ width: 26, height: 26, borderRadius: 26 / 2, backgroundColor: "#A8EFFE", alignItems: "center", justifyContent: "center" }}>
                    <Text style={[styles['text-label'], styles['color-navy-blue'], {}]}>{meeting.length}</Text>
                </View>
            </Animated.View>
         
          <Animated.ScrollView 
            // onScrollBeginDrag={_onScrollBeginDrag}
            showsVerticalScrollIndicator={false}
            onScroll={onScroll}
            scrollEventThrottle={16}
            bounces={false}
            contentContainerStyle={{ flexGrow: 1, paddingBottom: 200, marginTop: 100, backgroundColor: "#F7F8FE"}}>
               
                
                <View style={{ marginTop: 40, flex: 1 }}>
                    <Text style={[styles['text-header'], styles['color-navy-blue'], { marginLeft: 20 }]}>Today meetings</Text>
                    <FlatList
                        style={{ marginTop: 16 }}
                        data={getToday('today')}
                        ListEmptyComponent={() => {
                            return (
                                <Text style={[styles['text-label'], { marginLeft: 20 }]}>No today meeting</Text>
                            )
                        }}
                        renderItem={({ item, iindex }) => {

                            return (
                                <SubjectCard
                                    type="virtual"
                                    item={item}
                                    identifier="today"
                                    containerStyle={{
                                        width: width * .9, backgroundColor: "#FFCECE", paddingBottom: 70 }}
                                />
                            )
                        }}
                    />
                </View>

                <View style={{ marginTop: 40, flex: 1 }}>
                    <Text style={[styles['text-header'], styles['color-navy-blue'], { marginLeft: 20 }]}>Upcoming meetings</Text>
                    <FlatList
                        style={{ marginTop: 16 }}
                        data={getToday('upcoming')}
                        ListEmptyComponent={() => {
                            return(
                                <Text style={[styles['text-label'], {marginLeft: 20}]}>No upcoming meeting</Text>
                            )
                        }}
                        renderItem={({ item, iindex }) => {

                            return (
                                <SubjectCard
                                    type="virtual"
                                    item={item}
                                    identifier="upcoming"
                                    containerStyle={{ width: width * .9, backgroundColor: "#fff", paddingBottom: 20 }}
                                />
                            )
                        }}
                    />
                </View>

                <View style={{ marginTop: 40, flex: 1 }}>
                    <Text style={[styles['text-header'], styles['color-navy-blue'], { marginLeft: 20 }]}>Past meetings</Text>
                    <FlatList
                        style={{marginTop: 16}}
                        data={getToday('past')}
                        ListEmptyComponent={() => {
                            return (
                                <Text style={[styles['text-label'], { marginLeft: 20 }]}>No past meeting</Text>
                            )
                        }}
                        renderItem={({ item, iindex }) => {

                            return (
                                <SubjectCard
                                    type="virtual"
                                    item={item}
                                    identifier="past"
                                    containerStyle={{ width: width * .9, opacity: 0.5, backgroundColor:"#F8F8F8", paddingBottom: 20 }}
                                />
                            )
                        }}
                    />
                </View>
          </Animated.ScrollView>
           
        </SafeAreaView>
    )
}

export default Links
