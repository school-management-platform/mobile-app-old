import React, { useState, useEffect } from 'react'
import { View, Text, SafeAreaView, Dimensions, FlatList, TouchableOpacity } from 'react-native'
import { Header } from '../../components/Header'
import { styles } from '../../stylesheet/style'
import {SeatworkCard} from '../../components/Card'
import { ApiCall, baseUrl } from '../../utils/api'
import { useSelector } from 'react-redux'
import { LoadingModal } from '../../components/Modal'
const {width} = Dimensions.get('screen')
export const DocumentsFile = (props) => {
    const toggleDrawer = () => {
        props.navigation.toggleDrawer()
    }
    const [loading, setLoading] = useState(true)
    const [selectedLabel, setSelectedLabel] = useState(1)
    const subject = useSelector(state => state.user.selectedSubject)
    const UserData = useSelector(state => state.user)
    const district = useSelector(state => state.user.district)
    const [datas, setDatas] = useState({})

    const labels = [
        {
            id: 1, 
            name: "Power Point",
            color: "#FFCECE"
        },
        {
            id: 2,
            name: "Supplements",
            color:"#B5DAFF"
        },
        {
            id: 3,
            name: "Assessments",
            color:"#FFDB95"
        },
        {
            id: 4,
            name: "Activities",
            color:"#8DEFB6"
        }
    ]

    
    
    useEffect(() => {
        setLoading(true)
        if(selectedLabel == 1){
            let params = {
                subject_id: subject.id,
                type: 1,
            }
            ApiCall("/file/search", params, "get", UserData.details.token, district).then(res => {
                setLoading(false)
                setDatas(res.details.data.data)

            })
            .catch(error => {
                setLoading(false)
            })
        }
        else if(selectedLabel == 2){
            let params = {
                class_id: subject.class_id,
                type: 2,
            }
            ApiCall("/file/search/filtered", params, "get", UserData.details.token, district).then(res => {
                setLoading(false)
                
                setDatas(res.details.data.data)

            })
                .catch(error => {
                    setLoading(false)
                })
        }
        else if(selectedLabel == 3){
            let params = {
                class_id: subject.class_id,
                type: 3,
            }
            ApiCall("/file/search/filtered", params, "get", UserData.details.token, district).then(res => {
                setLoading(false)
                setDatas(res.details.data.data)

            })
                .catch(error => {
                    setLoading(false)
                })
        }
        else if(selectedLabel == 4){
            let params = {
                class_id: subject.class_id,
                is_student: 1
            }
            ApiCall("/activity/all", params, "get", UserData.details.token, district).then(res => {
                setLoading(false)
                
                setDatas(res.details.data.data)

            })
                .catch(error => {
                    setLoading(false)
                })
        }
        else{
            setLoading(false)
        }

        return(() => {

        })
    }, [selectedLabel, subject != null && subject.name])

    // let index = labels.findIndex(x => x.id == selectedLabel);

 

    

    
    return (
        <SafeAreaView style={[styles.Container]}>
            <LoadingModal
                show={loading}
            />
            <Header
                onToggle={toggleDrawer}
                containerStyle={{ position: 'relative', width: width }}
            />
            {
                !loading && (
                    <>
                        <View style={{ borderBottomWidth: 1, borderColor: "#E7ECF3" }}>
                         
                            <View style={{
                                padding: 20, flexDirection: "row", justifyContent: "space-between", backgroundColor: "#fff",
                                zIndex: 99, width: width,
                            }}>
                                <View>
                                    <Text style={[styles['text-header'], { fontWeight: '700', fontSize: 20 }]}>File Manager</Text>
                                    <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>{subject.name}</Text>
                                </View>

                                <View style={{ width: 26, height: 26, borderRadius: 26 / 2, backgroundColor: "#A8EFFE", alignItems: "center", justifyContent: "center" }}>
                                    <Text style={[styles['text-label'], styles['color-navy-blue'], {}]}>{selectedLabel == 4 ? datas.length : (datas.files != undefined  && datas.files.length)}</Text>
                                </View>
                            </View>

                            <FlatList
                                data={labels}
                                style={{ marginLeft: 10, paddingBottom: 20 }}
                                showsHorizontalScrollIndicator={false}
                                horizontal={true}
                                renderItem={({ item, index }) => {

                                    return (
                                        <TouchableOpacity onPress={() => setSelectedLabel(item.id)}>
                                            <View style={{ padding: 5, paddingHorizontal: 10, marginLeft: 10, backgroundColor: selectedLabel != item.id ? "#CCD1D9" : item.color, borderRadius: 8 }}>
                                                <Text style={[styles['text-label'], {}]}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                }}
                            />

                        </View>

                        <View style={{ flex: 1, backgroundColor: "#F7F8FE", alignItems: "center", justifyContent: "center", width: width }}>
                            <FlatList
                                data={datas.files == undefined ? datas : datas.files}

                                contentContainerStyle={{ flexGrow: 1, width: width, paddingBottom: 100 }}
                                renderItem={({ item, index }) => {

                                    return (
                                        <View style={{ marginTop: 20, alignSelf: "center" }}>
                                            <SeatworkCard
                                                type={selectedLabel}
                                                item={item}
                                               
                                                onPress={() => selectedLabel == 4 && props.navigation.navigate('Viewing', {
                                                    data: item
                                                })} />
                                        </View>
                                    )
                                }}
                            />
                        </View>
                    </>
                )
            }
        </SafeAreaView>
    )
}


