import React, {useEffect} from 'react'
import { View, Text, SafeAreaView, TouchableOpacity} from 'react-native'
import { WebView } from 'react-native-webview';
import { Header } from 'react-native/Libraries/NewAppScreen';
import { ArrowLeftSVG } from '../../components/IconSvg';
import Pdf from 'react-native-pdf';
import { useDispatch } from 'react-redux';
import { HIDE_NAV } from '../../Redux/actionType';
export const LinkViewing = (props) => {
    const link = props.route.params.link

    
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch({
            type: HIDE_NAV,
            data: true
        })
        
        return () => {
            dispatch({
                type: HIDE_NAV,
                data: false
            })
        }
    }, [])
    const toggleDrawer = () => {
        props.navigation.toggleDrawer()
    }
    return (
        <SafeAreaView style={{flex: 1}}>
            <TouchableOpacity 
                    onPress={() =>{
                    dispatch({
                        type: HIDE_NAV,
                        data: false
                    })
                    props.navigation.goBack()
                    }}
                    style={{padding: 20}}>
                <ArrowLeftSVG width={30} height={30} fill="#000"/>
            </TouchableOpacity>
            {/* <WebView
                source={{uri: link}}
                style={{flex: 1, width:"100%"}}
            /> */}

            <Pdf
                source={{uri: link}}
                onLoadComplete={(numberOfPages, filePath) => {
                    // 
                }}
                onPageChanged={(page, numberOfPages) => {
                    // 
                }}
                onError={(error) => {
                    // 
                    
                }}
                onPressLink={(uri) => {
                    // 
                }}
                style={{flex: 1}} />
        </SafeAreaView>
    )
}


