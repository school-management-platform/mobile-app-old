import moment from 'moment'
import React, {useState, useEffect} from 'react'
import { SafeAreaView, StyleSheet, Text, View, Dimensions, TouchableWithoutFeedback, Linking } from 'react-native'
import { ButtonComponent } from '../../components/Button'
import { ArrowLeftSVG, UploadSVG, CalendarSVG, TimeSVG, FileSVG, NotSubmittedSVG, SubmittedSVG, AttachSVG, CheckSVG } from '../../components/IconSvg'
import {styles} from '../../stylesheet/style'
const {width} = Dimensions.get('screen')
import DocumentPicker from 'react-native-document-picker';
import { ApiCall, ApiCallFormData } from '../../utils/api'
import { useSelector } from 'react-redux'
import { LoadingModal } from '../../components/Modal'
const DocumentViewing = (props) => {
    const UserData = useSelector(state => state.user)
    const [item, setItem] = useState(props.route.params.data)
    const district = useSelector(state => state.user.district)
    const [uploadedFile, setUploadedFile] = useState(null)
    const [loading, setLoading] = useState(false)


    useEffect(() => {

        getActivity()
    },[])
  

   const chooseFile = async() => {
       try {
           const res = await DocumentPicker.pick({
               type: [DocumentPicker.types.pdf, DocumentPicker.types.doc, DocumentPicker.types.docx, DocumentPicker.types.plainText, DocumentPicker.types.images],
           });
           
           setUploadedFile(res)
       } catch (err) {
           if (DocumentPicker.isCancel(err)) {
               // User cancelled the picker, exit any dialogs or menus and move on
           } else {
               throw err;
           }
       }
   }

   const getActivity = () => {
       setLoading(true)
       let body = {
           activity_id: item.id,
           is_student: 1
       }
       ApiCall('/activity/get', body, "get", UserData.details.token, district).then(res => {
            
           setItem(res.details.data.data)
           setLoading(false)
       }).catch(error => {
           setLoading(false)
       })
   }

   

 
   const submitFile = () => {
       setLoading(true)
       let formData = new FormData()
       formData.append('activity_id', item.id)
       formData.append('file', uploadedFile)
       formData.append('link', '-')

       ApiCallFormData('/activity/submit', formData, "post", UserData.details.token, district).then(res=> {
           setUploadedFile(null)
           setLoading(false)
       }).then(error => {
           setLoading(true)
       })
   }

   console.log(item)
    return (
        <SafeAreaView style={[styles.Container]}>

            <LoadingModal
                show={loading}
            />

            <View style={{flexDirection:"row", width, padding: 20, justifyContent:"space-between"}}>

            <TouchableWithoutFeedback onPress={() => props.navigation.goBack()}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <ArrowLeftSVG width={24} height={24} fill={styles['color-navy-blue'].color} />

                        <Text style={[styles['text-table-data'], { marginLeft: 12 }]}>Back</Text>
                    </View>
            </TouchableWithoutFeedback>
            
                <UploadSVG width={24} height={24} fill={styles['color-navy-blue'].color} />

            </View>

            <View style={{flex: 1, padding: 20  }}>
                <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color, fontSize: 20, lineHeight: 27 }]}>{item.title}</Text>


                <View style={{ flexDirection: "row", alignItems: "center", marginTop: 25 }}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <CalendarSVG height={16} width={16} fill={item.submitted_at == null ? "red" : styles['color-navy-blue'].color} />
                        <Text style={[styles['text-label'], { color: item.submitted_at == null ? "red" : styles['color-navy-blue'].color, marginLeft: 10 }]}>{item.deadline_date}</Text>
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center", marginLeft: 31 }}>
                        <TimeSVG height={16} width={16} fill={item.submitted_at == null ? "red" : styles['color-navy-blue'].color} />
                        <Text style={[styles['text-label'], { color: item.submitted_at == null ? "red" : styles['color-navy-blue'].color, marginLeft: 10 }]}>{moment("2020/05/05 " + item.deadline_time).format('hh:mmA')}</Text>
                    </View>
                </View>

                <View style={{ flexDirection: "row", alignItems: "center", marginTop: 12 }}>
                    <FileSVG height={16} width={16} fill={styles['color-navy-blue'].color} />
                    <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>PDF</Text>
                </View>

                <View style={{ marginTop: 20 }}>
                    <Text style={[styles['text-paragpraph'], { color: styles['color-grey-blue'].color }]}>{item.description}</Text>
                </View>

                {
                    item.submitted_at != null ? (
                        <>
                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: 12 }}>
                            <SubmittedSVG height={16} width={16} fill="#5AE579" />
                            <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{item.submitted_at}</Text>
                            {/* <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{moment(item.updated_at).format("MMMM DD, YYYY, HH:mmA")}</Text> */}
                        </View>

                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 12 }}>
                                <CheckSVG height={16} width={16} fill="#5AE579" />

                                <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{item.score + "/" + item.max_score}</Text>
                                {/* <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{moment(item.updated_at).format("MMMM DD, YYYY, HH:mmA")}</Text> */}
                            </View>
                         
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 17 }}>
                            
                                    <AttachSVG height={16} width={16} fill="#5AE579" />
                           

                               
                                        <TouchableWithoutFeedback onPress={async () => {
                                            

                                            props.navigation.navigate('Link', {
                                                link: item.file
                                            })
                                        }}>

                                            <Text style={[styles['text-mute'], { color: styles['color-blue'].backgroundColor, textDecorationLine: "underline", marginLeft: 12 }]}>View file</Text>
                                        </TouchableWithoutFeedback>
                               
                            </View>

                            {
                                item.teacher_file != null && (
                                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 17 }}>

                                        <AttachSVG height={16} width={16} fill="#5AE579" />



                                        <TouchableWithoutFeedback onPress={async () => {


                                            props.navigation.navigate('Link', {
                                                link: item.file
                                            })
                                        }}>

                                            <Text style={[styles['text-mute'], { color: styles['color-blue'].backgroundColor, textDecorationLine: "underline", marginLeft: 12 }]}>Teacher's file</Text>
                                        </TouchableWithoutFeedback>

                                    </View>
                                )
                            }

                          

                                {
                                    item.remarks != null && (
                                    <View style={{ marginTop: 20 }}>
                                        <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 5, fontWeight: "bold" }]}>Remarks:</Text>

                                        <Text style={[styles['text-label'], { color: styles['color-grey-blue'].color, marginLeft: 5 }]}>{item.remarks}Sample Remarks</Text>
                                        {/* <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{moment(item.updated_at).format("MMMM DD, YYYY, HH:mmA")}</Text> */}
                                    </View>
                                    )
                                }
                        </>
                    )
                        :
                        (
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 12 }}>
                                <NotSubmittedSVG height={16} width={16} fill="#E84A5F" />
                                <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>Not yet submitted</Text>
                                {/* <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{moment(item.updated_at).format("MMMM DD, YYYY, HH:mmA")}</Text> */}
                            </View>
                        )
                }
            </View>

      {
          item.status == 0 || item.turn_in == 1 || item.score == null && (
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                        <ButtonComponent
                            onPress={chooseFile}
                            textColor={{ color: styles['color-blue'].backgroundColor }}
                            containerStyle={[, { backgroundColor: "#fff", borderWidth: 1, borderColor: styles['color-blue'].backgroundColor, width: "90%", borderRadius: 8, marginTop: 40 }]}
                            text={"Upload File"} />

                        <ButtonComponent
                            onPress={submitFile}
                            disabled={uploadedFile != null ? false : true}
                            containerStyle={[, { backgroundColor: styles['color-blue'].backgroundColor, width: "90%", borderRadius: 8, marginTop: 14, opacity: uploadedFile != null ? 1 : 0.5 }]}
                            text={"Submit Activity"} />
                    </View>
          )
      }


        </SafeAreaView>
    )
}

export default DocumentViewing


