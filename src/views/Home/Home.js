import axios from 'axios'
import moment from 'moment'
import React, { useRef, useState, useEffect } from 'react'
import { View, Text, SafeAreaView, FlatList, ScrollView, Animated, TouchableOpacity, BackHandler, Alert } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { AnnouncementCard, CardQuote, ScheduleCard, SubjectCard } from '../../components/Card'
import { Header } from '../../components/Header'
import { AnnouncementSVG, ParentsSVG, StudentSVG } from '../../components/IconSvg'
import { Intro } from '../../components/Intro'
import { TimeInModal, LoadingModal } from '../../components/Modal'
import { SET_LOGOUT, SET_USER, UPDATE_USER } from '../../Redux/actionType'
import { styles } from '../../stylesheet/style'
import { ApiCall, baseUrl, baseurl2 } from '../../utils/api'

export const Home = (props) => {
    const dispatch = useDispatch()
    const district = useSelector(state => state.user.district)

    
    const UserData = useSelector(state => state.user)
    const subject = useSelector(state => state.user.selectedSubject)
    const scrollIndicator = useRef(new Animated.Value(20)).current;
    const [loading, setLoading] = useState(true)
    const [completeScrollBarHeight, setCompleteScrollBarHeight] = useState(1);
    const [visibleScrollBarHeight, setVisibleScrollBarHeight] = useState(0);
    const [meeting, setMeeting] = useState([])
    const [settings, setSettttings] = useState({})
    const [announcement, setAnnouncement] = useState([])
    const [schedule, setSchedule] = useState([])

    
    const scrollIndicatorSize =
        completeScrollBarHeight > visibleScrollBarHeight
            ? (visibleScrollBarHeight * visibleScrollBarHeight) /
            completeScrollBarHeight
            : visibleScrollBarHeight;

    const difference =
        visibleScrollBarHeight > scrollIndicatorSize
            ? visibleScrollBarHeight - scrollIndicatorSize
            : 1;

    const scrollIndicatorPosition = Animated.multiply(
        scrollIndicator,
        visibleScrollBarHeight / completeScrollBarHeight
    ).interpolate({
        inputRange: [0, difference],
        outputRange: [20, difference - 20],
        extrapolate: 'clamp'
    });

    

    useEffect(() => {
        callAllApi()
        return () => {
          
        }
    }, [subject != null && subject.name])


    useEffect(() => {
        const backAction = () => {
            Alert.alert("Hold on!", "Are you sure you want to logout?", [
                {
                    text: "Cancel",
                    onPress: () => null,
                    style: "cancel"
                },
                { text: "YES", onPress: () => {
                    ApiCall('/auth/logout', null, "post", UserData.details.token).then(res => {

                        props.navigation.navigate('Auth')
                        setTimeout(() => {
                            dispatch({
                                type: SET_LOGOUT,

                            })
                        }, 500)

                    })
                } }
            ]);
            return true;
        };

        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
        );

        return () => backHandler.remove();
    }, []);




    const callAllApi = () => {
        
        let virtualParams = {
            class_id: UserData.selectedSubject.class_id,
            section_id: UserData.selectedSubject.section_id
        }

        let settingParams = {
            class_id: UserData.selectedSubject.class_id,
        }

        let sectionParams = {
            id: UserData.details.record[0].section_id
        }
        setLoading(true)
        const virtualLink = axios.get((district == 1 ? baseUrl : baseurl2) + "/meeting/all", { params: virtualParams, headers: { "AUTHORIZATION": `Bearer ${UserData.details.token}` }})
        const setting = axios.get((district == 1 ? baseUrl : baseurl2) + "/settings", { params: settingParams, headers: { "AUTHORIZATION": `Bearer ${UserData.details.token}` } })
        const section = axios.get((district == 1 ? baseUrl : baseurl2) + "/section/get", { params: sectionParams, headers: { "AUTHORIZATION": `Bearer ${UserData.details.token}` } })
        const userData = axios.get((district == 1 ? baseUrl : baseurl2) + "/student/get/current", { params: sectionParams, headers: { "AUTHORIZATION": `Bearer ${UserData.details.token}` } })


        axios.all([virtualLink, setting, section, userData]).then(axios.spread(async(...responses) => {
            const responseOne = responses[0]
            const responseTwo = responses[1]
            const responseThree = responses[2]
            const responseFoure = responses[3]

    
            dispatch({
                type: UPDATE_USER,
                data: responseFoure.data.data
            })
            


            let initialAnn = []

            initialAnn.push({ ...responseTwo.data.data.announcement, img: require("../../assets/card-1.png"),name: "Announcement" ,icon: <AnnouncementSVG fill={"#fff"} />})
            initialAnn.push({ ...responseTwo.data.data.student_manual, img: require("../../assets/card-2.png"), name: "Student's Manual", icon: <StudentSVG fill={"#fff"} />})
            initialAnn.push({ ...responseTwo.data.data.parent_manual, img: require("../../assets/card-3.png"), name: "Parent's Manual", icon: <ParentsSVG fill={"#fff"} />})
            setSettttings(responseTwo.data.data)
            setSchedule(responseThree.data.data)
            setMeeting(responseOne.data.data)
            setAnnouncement(initialAnn)
        

            setTimeout(() => {
                setLoading(false)
            }, 1000)
        })).catch(error => {
            
            Alert.alert("Warning!", "Session expired.", [
                {
                    text: "Login again", onPress: () => {
                        ApiCall('/auth/logout', null, "post", UserData.details.token).then(res => {

                            
                            props.navigation.navigate('Auth')
                            setTimeout(() => {
                                store.dispatch({
                                    type: SET_LOGOUT,

                                })

                            }, 1500)

                        })
                    }
                }
            ]);
        })

    }


    
    



    const toggleDrawer = () => {
        props.navigation.toggleDrawer()
    }
    
    
    return (
        <SafeAreaView style={[styles.Container, { backgroundColor: "#fff" }]}>
                <TimeInModal
                    show={false}
                />
                <LoadingModal
                    show={loading}
                />
            {
                !loading && (
                    <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1 }}>

                        <Header user={UserData.details.student} onToggle={toggleDrawer} />

                        <Intro containerStyle={{ marginTop: 56 }} />


                        <View style={{ paddingHorizontal: 20, marginTop: 40 }}>
                            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                <Text style={[styles['text-header'], { fontSize: 20, lineHeight: 27 }]}>Virtual Links</Text>
                               <TouchableOpacity onPress={() => props.navigation.navigate('Links')}>
                                    <Text style={[styles['text-table-data'], { color: styles['color-blue'].backgroundColor }]}>See more</Text>
                               </TouchableOpacity>
                            </View>
                            <Text style={[styles['text-muted'], { fontSize: 14, color: styles['color-grey-blue'].color }]}>{"Today, " + moment().format("MMMM DD, YYYY")}</Text>

                        </View>

                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            style={{ flexGrow: 0, marginTop: 16, marginBottom: 60 }}
                            keyExtractor={(item, index) => String(index)}
                            horizontal={true}
                            data={meeting}
                            renderItem={SubjectCard}
                        />

                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            style={{ flexGrow: 0, marginBottom: 60 }}
                            keyExtractor={(item, index) => String(index)}
                            horizontal={true}
                            data={announcement}
                            renderItem={(val) => AnnouncementCard(val, props.navigation)}
                        />

                        <View style={{ marginLeft: 20 }}>
                            <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color, fontSize: 20, fontWeight: "700", lineHeight: 27 }]}>Your Schedule</Text>
                        </View>

                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            style={{ flexGrow: 0, marginTop: 24, marginBottom: 16 }}
                            keyExtractor={(item, index) => String(index)}
                            horizontal={true}
                            data={schedule.schedule}
                            renderItem={(data) => ScheduleCard(data, schedule.schedule.length)}
                            onContentSizeChange={(width, height) => {
                                setCompleteScrollBarHeight(width);
                            }}
                            onLayout={({
                                nativeEvent: {
                                    layout: { height, width }
                                }
                            }) => {
                                setVisibleScrollBarHeight(width);
                            }}
                            onScroll={Animated.event(
                                [{ nativeEvent: { contentOffset: { x: scrollIndicator } } }],
                                { useNativeDriver: false }
                            )}
                            scrollEventThrottle={16}

                        />

                        {schedule.schedule.length > 1 && (
                            <Animated.View
                                style={{
                                    height: 8,
                                    borderRadius: 50,
                                    backgroundColor: '#A4ADBB',
                                    width: scrollIndicatorSize,
                                    transform: [{ translateX: scrollIndicatorPosition }]
                                }}
                            />
                        )}

                        <View style={[{ marginTop: 60, marginBottom: 100 }]}>
                            <CardQuote text={settings.quotes} />
                        </View>




                    </ScrollView>
                )
            }
        </SafeAreaView>
    )
}


