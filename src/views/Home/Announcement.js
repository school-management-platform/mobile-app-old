import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, Image, Dimensions } from 'react-native'
import { Header } from '../../components/Header'
import { AnnouncementSVG } from '../../components/IconSvg'
import { styles } from '../../stylesheet/style'
const {width} = Dimensions.get('screen')
export const Announcement = (props) => {
    let bottomImage
    const [prevent, setPrevent] = useState(false)
    const [label, setLabel] = useState('Announcement')
    const data = props.route.params.data

    
    const goBack = () => {
        props.navigation.goBack()
        setPrevent(true)
    }

    useEffect(() => {
        setPrevent(false)

        if (data.id == "announcement") {
            
            setLabel('Announcement')

        }
        else if (data.id == "student_manual") {
            
            setLabel('Student Manual')

        }
        else if (data.id == "parent_manual") {
            
            setLabel('Parent Manual')
        }
    }, [])



    if(data.id == "announcement"){
        bottomImage = require('../../assets/Vector_Image.png')
        
    
    }
    else if (data.id == "student_manual") {
        bottomImage = require('../../assets/Vector_Image2.png')
        

    }
    else if (data.id == "parent_manual") {
        bottomImage = require('../../assets/Vector_Image3.png')
        
    }

    
    return (
        <SafeAreaView style={[styles.Container]}>

            <Header
                type={1}
                onBack={goBack}
                disablePress={prevent}
                rightIcon={data}
            />

            <View style={[styles.Container, { paddingHorizontal: 20 }]}>
                <View>
                    <Text style={[styles['text-header'], { fontWeight: "700", marginTop: 46, fontSize: 20 }]}>{label}</Text>
                    <Text style={[styles['text-mute'], {color:styles['color-grey-blue'].color}]}>February 22, 2021</Text>
                </View>

                <View style={{marginTop: 24}}>
                    <Text style={[styles['text-paragpraph'], {lineHeight: 21, fontSize: 16}]}>
                         {data.message} 
                    </Text>
                </View>

             
            </View>
            <Image
                resizeMode="stretch"
                style={{height: 300, width: width}}
                source={bottomImage}
            />
        </SafeAreaView>
    )
}

