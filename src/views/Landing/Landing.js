import React, { useState, useEffect } from 'react'
import { View, Alert,Text, ImageBackground, SafeAreaView, Dimensions, FlatList, TextInput, KeyboardAvoidingView, Platform, ScrollView } from 'react-native'
import { color } from 'react-native-reanimated';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { ButtonComponent } from '../../components/Button';
import { Card } from '../../components/Card';
import { LogoSVG } from '../../components/IconSvg';
import { LoginModal, LoadingModal, SchoolLoginModal } from '../../components/Modal';
import { SET_LOADING, SET_SUCCESS, SHOW_CHANGE, SET_DISTRICT } from '../../Redux/actionType';
import { styles } from "../../stylesheet/style";
import { ApiCall, baseUrlSchoolLogo } from '../../utils/api';
const { width, height } = Dimensions.get('screen')

const Landing = (props) => {

    // bCc*SiNCe1947
    // franzloren10@gmail.com
    // 20080610

    const dispatch = useDispatch()
    const loading = useSelector(state => state.loadingVisible.loading)
    const relog = useSelector(state => state.user.remember)
    const district = useSelector(state => state.user.district)

    const [showLogin, setShowLogin] = useState(false)
    const [selectedSchool, setSelectedSchool] = useState('')
    const [data, setData] = useState([])
    const [consistentData, setConsistentData] = useState([])
    const [search, setSearch] = useState('')
    const [schoolLogin, setSchoolLogin] = useState(false)
    const [credentials, setCredentials] = useState('')
    const [schoolToLog, setSchoolToLog] = useState('')
    const [counter, setCounter] = useState(0)

    
    useEffect(() => {
    
        if (relog) {
            props.navigation.navigate('HomeDrawer')
        }else{
            Alert.alert("Welcome", "Please choose your district.", [
                {
                    text:"District 1",
                    onPress:() => {
                        dispatch({
                            type: SET_DISTRICT,
                            data: 1
                        })
                        setCounter(counter + 1)
                    }
                },
                {
                    text: "District 2",
                    onPress: () => {
                        dispatch({
                            type: SET_DISTRICT,
                            data: 2
                        })
                        setCounter(counter + 1)
                    }
                }
            ])
        }
    },[relog])
    
    useEffect(() => {
       
        dispatch({
            type: SET_LOADING,
            data: false
        })
        dispatch({
            type: SHOW_CHANGE,
            data: false
        })
        ApiCall('/pub/school/all', null, "get", null, district).then(res => {
            
            if(res.status == 200){
                setData(res.details.data.data)
                setConsistentData(res.details.data.data)
            }
            else{
                setData([])
            }
        }).then(res => {
            
        })
    }, [counter])

    

    useEffect(() => {
        
        // props.navigation.navigate('HomeDrawer')
    },[loading])

    
    const cardSelected = (data) => {


        setSelectedSchool(data)
        setSchoolLogin(true)
        

       
      
    }


    const renderSchool = ({ item, index }) => {
        
        let length = data.length

        return (
            <View style={{marginRight: index + 1 == length ? 20 : 0}}>
                <Card
                    cardPress={() => cardSelected(item)}
                    item={item} index={index} />
            </View>
        )
    }

    const login = () => {
            setShowLogin(false)
            props.navigation.navigate('HomeDrawer')
    }   


    const filterSchool = (v) => {
        
        let initial = data
        let result = initial.filter(item => {
            let name = item.name.toLowerCase()
            return name.includes(v.toLowerCase())
        })
     
            setData(result)
        if(v == ""){
            setData(consistentData)
        }
        setSearch(v)
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsHorizontalScrollIndicator={false}>
        <ImageBackground
            source={require('../../assets/Background_image.png')}
            resizeMode="stretch"
            style={{
                width: width,
                
                flex: 1
            }}
        >
            {/* <LoadingModal
                show={loading}
            /> */}
            <SchoolLoginModal
                    district={district}
                    closeModal={() => setSchoolLogin(false)}
                    passData={(val) => {
                        
                        setSchoolToLog(val)
                        setSchoolLogin(false)
                        setTimeout(() => {
                            setShowLogin(true)
                        }, 500)
                    }}
                    onPress={login}
                    data={selectedSchool}
                    close={() => setSchoolLogin(false)}
                    imgSrc={selectedSchool != "" ? selectedSchool.img : null}
                    schoolName={selectedSchool != "" ? selectedSchool : ""}
                    avatar={baseUrlSchoolLogo + "/" +selectedSchool.logo}
                    visible={schoolLogin}
            />
            {
                showLogin && (
                        <LoginModal
                            district={district}
                            closeModal={() => setShowLogin(false)}
                            onPress={login}
                            close={() => setShowLogin(false)}
                            imgSrc={schoolToLog != "" ? schoolToLog.img : null}
                            schoolName={schoolToLog != "" ? schoolToLog : ""}
                            visible={showLogin} />

                )
            }
            <KeyboardAvoidingView
                keyboardVerticalOffset={50}
                behavior={Platform.OS == "ios" && "padding"}
                style={{ flex: 1 }}>

                    <View style={{ alignItems: "center" }}>
                        <View style={{ backgroundColor: "#fff", width: width, alignItems: "center", paddingBottom: 10, paddingTop: 20,  }}>
                            <View style={{ marginTop: Platform.OS == "ios" ? 20 : 20}}>

                            </View>
                            <LogoSVG
                                width={150}
                                height={150}
                            />

                            <Text style={[styles['text-header'], { fontSize: 32, lineHeight: 43, fontWeight: "700", marginTop: 7 }]}>MaPSA - LMS</Text>
                        </View>

                        <Text style={[styles['text-mute'], { color: styles['color-white'].color, textAlign: "center", paddingHorizontal: 40, marginTop: 40 }]}>
                            Welcome to Diocese School of Antipolo’s Learning Management System. Manage all of your learning activities and meetings through MaPSA LMS
                        </Text>

                    </View>
             
              
                {
                    district != 0 && (
                             <SafeAreaView style={[styles.Container, {backgroundColor:null}]}>

          


                        <View style={{ marginTop: 50, alignItems: "center" }}>

                            <ButtonComponent
                                onPress={() => {
                                            Alert.alert("Welcome", "Please choose your district.", [
                                                {
                                                    text: "District 1",
                                                    onPress: () => {
                                                        dispatch({
                                                            type: SET_DISTRICT,
                                                            data: 1
                                                        })
                                                        setCounter(counter + 1)
                                                    }
                                                },
                                                {
                                                    text: "District 2",
                                                    onPress: () => {
                                                        dispatch({
                                                            type: SET_DISTRICT,
                                                            data: 2
                                                        })
                                                        setCounter(counter + 1)
                                                    }
                                                }
                                            ])
                                }}
                                textColor={{color:"#000"}}
                                containerStyle={{backgroundColor:"#fff", width: null, paddingHorizontal: 20, alignSelf:"flex-start", marginLeft: 20}}
                                text={"District " + district}
                            />
                            <Text style={[styles['text-mute'], { fontSize: 20, color: styles['color-white'].color, alignSelf: "flex-start", marginLeft: 20, marginTop: 20 }]}>Select your school</Text>
                            <FlatList
                                showsHorizontalScrollIndicator={false}
                                keyExtractor={(item, index) => String(index)}
                                style={{ marginTop: 20, flexGrow: 0 }}
                                // contentContainerStyle={{flexGrow: 0}}
                                horizontal={true}
                                data={data}
                                renderItem={renderSchool}
                            />
                            <Text style={[styles['text-label'], { color: styles['color-white'].color, marginTop: 20 }]}>OR</Text>
                            <TextInput
                                value={search}
                                onChangeText={(v) => filterSchool(v)}
                                placeholder="Search school..."
                                placeholderTextColor="#c5c5c5"
                                style={[styles['text-input-style'], { width: width * .9, marginTop: 20, color:"#000" }]}
                            />
                        </View>


                        <Text></Text>

                    </SafeAreaView> 
                    )
                }
           
            </KeyboardAvoidingView>
          
        </ImageBackground>
        </ScrollView>
    )
}

export default Landing
