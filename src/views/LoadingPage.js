import React, { Component } from 'react'
import { Text, View, SafeAreaView, ImageBackground, Image, Dimensions, ActivityIndicator, Platform } from 'react-native'
import { LogoSVG } from '../components/IconSvg'
import { styles } from '../stylesheet/style'
const {width, height} = Dimensions.get('screen')
export const LoadingPage = (props) => {
    
        return (
            <ImageBackground 
                source={require('../assets/Background_image.png')}
                resizeMode="stretch"
                style={{
                    width: width,
                    height: height
                }}
            style={[styles.Container]}>

                <View style={{ backgroundColor: "#fff", width: width, alignItems: "center", paddingBottom: 10, paddingTop: 20, }}>
                    <View style={{ marginTop: Platform.OS == "ios" ? 20 : 20 }}>

                    </View>
                    <LogoSVG
                        width={150}
                        height={150}
                    />


                </View>
             
                <ActivityIndicator style={{ marginTop: 100 }} size="large" color="#fff" />
         

         
            </ImageBackground>
        )
    
}


