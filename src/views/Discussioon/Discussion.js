import React, { Component, useEffect, useState, useRef } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Dimensions, TextInput, TouchableOpacity, KeyboardAvoidingView, Platform, Alert } from 'react-native';
import { styles } from '../../stylesheet/style';
import {ArrowLeftSVG, ChatSVG, SendSVG} from '../../components/IconSvg'
import { UserThread } from '../../components/UserThread';
import { FlatList } from 'react-native-gesture-handler';
const {width} = Dimensions.get('screen')
import { useSelector } from 'react-redux'
import { ApiCall } from '../../utils/api';
export const Discussion = props => {
    const inputRef = useRef(null)
    const swipeRef = useRef({})
    const UserData = useSelector(state => state.user)
    const subject = useSelector(state => state.user.selectedSubject)
    const district = useSelector(state => state.user.district)
    const [data, setData] = useState([])
    const [reply, setReply] = useState(false)
    const [message, setMessage] = useState('')
    const [counter, setCounter] = useState(0)
    const [activeItem, setActiveItem] = useState(null)

    useEffect(() => {
    
        getDiscuussion()
    return(() => {

    })        

    }, [subject != null && subject.name])

    const getDiscuussion = () => {

        let params = {
            class_id: UserData.selectedSubject.class_id,
        }
        ApiCall('/comment/list' , params, "get", UserData.details.token, district).then(res => {
            
            if (res.status == 200) {
                setData(res.details.data.data)
            }
            else {

            }
        })

    }

    

    const sendMessage = () => {
        let params = {
            class_id: UserData.selectedSubject.class_id,
            section_id: UserData.details.record[0].section_id,
            message: message
        }

        if(reply){
                params = {
                class_id: UserData.selectedSubject.class_id,
                section_id: UserData.details.record[0].section_id,
                message: message,
                reply_to: activeItem
            }
        }
        else{
               params = {
                class_id: UserData.selectedSubject.class_id,
                section_id: UserData.details.record[0].section_id,
                message: message,
            }
        }

        

        ApiCall('/comment/store', params, "post", UserData.details.token, district).then(res => {
                
            if (res.status == 200) {
                getDiscuussion()
                setReply(false)
                setMessage('')
                swipeRef.current[activeItem].close()
                setActiveItem(null)
            }
            else {
                Alert.alert('Warning!', "Unable to send message", [
                    {
                        text:"Please try again.",
                        onPress: () => {
                            setReply(false)
                            setMessage('')
                        }
                    }
                ])
            }
           
        })
    }

    const replySent = (v, type) => {
       
        inputRef.current.focus()
        setReply(true)
  
            
            let finalId = v.id

            

            if (activeItem != null && activeItem != finalId) {
                swipeRef.current[activeItem].close()
            }

            

            setActiveItem(finalId)


        
        
    }

    

    return(
        <SafeAreaView style={[styles.Container]}>
            <KeyboardAvoidingView 
                behavior={Platform.OS === "ios" ? "padding" : "height"}
                enabled style={styles.Container}>
                <View style={{ flexDirection: "row", width, padding: 20, justifyContent: "space-between" }}>

                    <TouchableWithoutFeedback onPress={() => props.navigation.goBack()}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <ArrowLeftSVG width={24} height={24} fill={styles['color-navy-blue'].color} />

                            <Text style={[styles['text-table-data'], { marginLeft: 12 }]}>Back</Text>
                        </View>
                    </TouchableWithoutFeedback>

                    <ChatSVG width={24} height={24} fill={styles['color-navy-blue'].color} />

                </View>

                <View style={{ padding: 20 }}>
                    <Text style={[styles['text-paragpraph'], { fontWeight: "700", fontSize: 20, lineHeight: 27 }]}>{subject.name}</Text>
                    <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>Discussion Board</Text>
                </View>



                <FlatList
                    data={data}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 50 }}
                    renderItem={({ item, index }) => {

                        return (
                            <View style={{ marginTop: 24 }}>
                                <UserThread
                                    reload={() => setCounter(counter + 1)}
                                    swipeRef={(val, el) => el != null && (swipeRef.current[val.id] = el )}
                                    reply={(v, type) => replySent(v, type)} 
                                    type={1}
                                    index={index}
                                    item={item}
                                />
                            </View>
                        )
                    }}
                />

                <View style={{ width, backgroundColor: "#E7ECF3", padding: 20, flexDirection: "row", alignItems: "center" }}>

                    <TextInput
                        ref={inputRef}
                        value={message}
                        onChangeText={(val) => setMessage(val)}
                        placeholder="Enter message here..."
                        placeholderTextColor="#c5c5c5"
                        style={{ padding: 20, backgroundColor: "#F7F8FE", borderRadius: 8, width: "90%", color:"#000" }}
                    />

                    <TouchableOpacity onPress={sendMessage}>
                        <View style={{ marginLeft: 12 }}>
                            <SendSVG width={24} height={24} fill={styles['color-navy-blue'].color} />
                        </View>
                    </TouchableOpacity>

                </View>

            </KeyboardAvoidingView>
    
        </SafeAreaView>
    )
}