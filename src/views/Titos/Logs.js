import moment from 'moment'
import React, {useEffect, useState} from 'react'
import { View, Text, SafeAreaView, Image, TouchableWithoutFeedback, Dimensions, Alert, ScrollView } from 'react-native'
import { useSelector } from 'react-redux'
import { ArrowLeftSVG } from '../../components/IconSvg'
import {styles} from '../../stylesheet/style'
import { ApiCall } from '../../utils/api'
const {width} = Dimensions.get('screen')
export const Logs = (props) => {
    const UserData = useSelector(state => state.user)
    const subject = useSelector(state => state.user.selectedSubject)
    const district = useSelector(state => state.user.district)
    const [logData, setLogData] = useState([])


    useEffect(() => {
        getLogs()
    }, [subject != null && subject.name])

    const getLogs = () => {


        let params = {
            class_id: subject.class_id
        }

        ApiCall('/student/attendances', params, "get", UserData.details.token, district).then(res => {

            if (res.status == 200) {
          
                let result = res.details.data.data
                   let firstFive = result.slice(0, 5)
             
                    setLogData([...firstFive])
            }
            else {
                Alert.alert('Warning!', "Unable to get logs", [
                    {
                        text: "Please try again.",
                        onPress: () => {
                            
                        }
                    }
                ])
            }

        })
    }

    
    return (
        <SafeAreaView style={[styles.Container]}>
            <View style={{ flexDirection: "row", width, padding: 20, justifyContent: "space-between" }}>

                <TouchableWithoutFeedback onPress={() => props.navigation.goBack()}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <ArrowLeftSVG width={24} height={24} fill={styles['color-navy-blue'].color} />

                        <Text style={[styles['text-table-data'], { marginLeft: 12 }]}>Back</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>

           <ScrollView contentContainerStyle={{flexGrow: 1}}>
            

                <View style={{ padding: 20, marginTop: 25 }}>
                    <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color, fontWeight: "700", fontSize: 20 }]}>Time logs</Text>
                    <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>Most recet (5) time logs...</Text>
                </View>

                <View style={{ width: width * .9, flexDirection: "row", justifyContent: "space-between", alignSelf: "center" }}>
                    <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>Date and Time</Text>
                    <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>Type</Text>
                </View>




                <View style={{ marginTop: 18 }}>
                    {logData.map((item, index) => {
                        return (
                            <View style={{ width: width * .9, flexDirection: "row", justifyContent: "space-between", alignSelf: "center", marginTop: 10 }}>
                                <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color }]}>{moment(item.updated_at).format('MMMM DD, HH:mm:ss A')}</Text>

                                <View style={{ paddingVertical: 7, paddingHorizontal: 15, borderRadius: 8, backgroundColor: item.status == 0 ? "#CCD1D9" : styles['color-primary'].color }}>
                                    <Text style={[styles['text-mute'], { color: item.status == 0 ? "#000" : "#fff" }]}>{item.status == 1 ? "In" : "Out"}</Text>
                                </View>
                            </View>
                        )
                    })}
                </View>


                <View style={{ padding: 20, marginTop: 28 }}>
                    <Text style={[styles['text-mute'], { color: styles['color-navy-blue'].color, textAlign: "center" }]}>If you have encounter a problem in your Time logs, please contact your teacher for this subject.</Text>
                </View>


                <Image
                    style={{width: width}}
                    source={require('../../assets/Vector_Robot.png')}
                
                />


           </ScrollView>
        </SafeAreaView>
    )
}


