import React, { useState } from 'react'
import { Modal, Text, TouchableWithoutFeedback, View, StyleSheet, TextInput, Dimensions, Image, Button, TouchableOpacity, Animated, SafeAreaView, ImageBackground, ActivityIndicator, Alert, KeyboardAvoidingView } from "react-native";
import { styles } from '../stylesheet/style';
import { ButtonComponent } from '../components/Button';
import { ArrowLeftSVG, ClockSVG, CloseSVG, ShowSVG, TimeSVG } from '../components/IconSvg';
import moment from 'moment';
import { ApiCall } from '../utils/api';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { LOG_IN, REMEMBER, SCHOOL_LOGIN, SET_LOADING, SET_SUCCESS, SET_USER } from '../Redux/actionType';
import {store} from '../Redux/confiigureStore'
const { height, width } = Dimensions.get('screen')
let stored = store.getState()
export const LoginModal = (props) => {
    const loginDetails = useSelector(state => state.user)
    const district = props.district
    const dispatch = useDispatch()
   const navigation = useNavigation()
   const [showPass, setShowPass] = useState(false)
    const [passwordInputHeight, setPasswordInputHeight] = useState(null)
    const [log, setLog] = useState({
        email: '',
        password: ''
        // 
    // 
    })
    // email: 'franzloren10@gmail.com,
    //     password: '20080610'
    // email: 'johnadoe@gmail.com',
    //     password: 'password'
    const login = (data) => {
        props.closeModal(false)
        dispatch({
            type: SET_LOADING,
            data: true
        })

        navigation.navigate('Loading')

        let body = {
            email: loginDetails.email,
            password: loginDetails.password,
            school_token: props.schoolName.school_token,
            role_id: 4
        }

        

        ApiCall('/auth/login', body, "post", null, district).then(res => {
            console.log(res, "--> WAIT WHAT")
            if (res.status == 200) {
                dispatch({
                    type: SET_LOADING,
                    data: false
                })
                dispatch({
                    type: SET_SUCCESS,
                    data: true
                })
          
                dispatch({
                    type: SET_USER,
                    data: res.details.data.data
                })

                dispatch({
                    type: REMEMBER,
                    data: true
                })
                navigation.navigate('HomeDrawer')
                props.closeModal(false)
                
            }
            else {
                console.log(res, "--> HMMP")
                Alert.alert('Warning', 'Something went wrong.', [
                    {
                        text:"Try again",
                        onPress: () => {
                            dispatch({
                                type: SET_SUCCESS,
                                data: false
                            })
                            navigation.goBack()
                        }
                    }
                ])
       
               
            }
        })
    }

    
    return (


        <Modal
            animationType="slide"
            transparent={true}
            presentationStyle="overFullScreen"
            visible={props.visible}
        >
            <KeyboardAvoidingView enabled behavior="padding" keyboardVerticalOffset={-100}  style={{ flex: 1, alignItems: "center", backgroundColor: 'transparent', justifyContent: "flex-end" }}>
                <View style={[{ backgroundColor: "#fff", height: height * .55, width: width, paddingHorizontal: 30 }, modalStyle.borderTop]}>

                    <View style={{ flexDirection: "row", marginTop: 20, alignItems: "center" }}>

                        <TouchableOpacity onPress={() => {
                            dispatch({
                                type: LOG_IN,
                                email: "",
                                password: ""

                            })
                            props.close()
                        }}>

                            <ArrowLeftSVG
                                width={styles['icon-size'].width}
                                height={styles['icon-size'].height}
                                fill={styles['color-navy-blue'].color}
                            />

                        </TouchableOpacity>
                        <Text style={[styles['text-table-data'], { marginLeft: 12, color: styles['color-grey-blue'].color }]}>
                            Back to School selection
                        </Text>
                    </View>

                    <View style={{ flexDirection: "row", marginTop: 28, alignItems: "center" }}>
                        <Image resizeMode="contain" style={{ height: 33, width: 33 }} source={props.imgSrc} />
                        <Text style={[styles['text-header'], { marginLeft: 8 }]}>{props.schoolName != "" && props.schoolName.school.name}</Text>
                    </View>

                    <View style={{ marginTop: 16 }}>
                        <TextInput
                            placeholder="Email"
                            value={loginDetails.email}
                            autoCapitalize="none"
                            onChangeText={(v) => {
                                setLog({
                                    ...log,
                                    email: v
                                })

                  

                                dispatch({
                                    type: LOG_IN,
                                    email: v,
                                    password: loginDetails.password

                                })
                            }}
                            placeholderTextColor="#c5c5c5"
                            style={[styles['text-input-style'], styles['with-border'], { width: "100%", marginTop: 20, color:"#000" }]}
                        />

                        <View onLayout={(evt) => {

                            setPasswordInputHeight(evt.nativeEvent.layout.height)
                        }}>
                            <TextInput
                                secureTextEntry={showPass ? false : true}
                                value={loginDetails.password}
                                onChangeText={(v) => {
                                    setLog({
                                        ...log,
                                        password: v
                                    })

                                 
                                    dispatch({
                                        type: LOG_IN,
                                        email: loginDetails.email,
                                        password: v
                                        
                                    })
                                }}
                                placeholder="Password"
                                placeholderTextColor="#c5c5c5"
                                style={[styles['text-input-style'], styles['with-border'], { width: "100%", marginTop: 24, color:"#000" }]}
                            />
                            <TouchableWithoutFeedback onPress={() => showPass ? setShowPass(false) : setShowPass(true)}>
                                <View style={{ position: "absolute", right: 10, top: passwordInputHeight != null ? passwordInputHeight / 2 : 10 }}>
                                    <ShowSVG />
                                </View>
                            </TouchableWithoutFeedback>
                        </View>

                        
                    </View>

                    

                    <View style={{ alignItems: "center" }}>
                        <ButtonComponent
                            onPress={login}
                            containerStyle={[, { backgroundColor: styles['color-blue'].backgroundColor, width: "100%", borderRadius: 8, marginTop: 40 }]}
                            text="Sign in" />

                        {/* <Text style={[styles['text-mute'], { color: styles['color-blue'].backgroundColor, marginTop: 16 }]}>Forgot Password?</Text> */}
                    </View>
                </View>

            </KeyboardAvoidingView>
        </Modal>

    )
}

export const SchoolLoginModal = (props) => {
    const dispatch = useDispatch()
    const district = props.district
    const navigation = useNavigation()
    const [showPass, setShowPass] = useState(false)
    const pass = useSelector(state => state.user.school_password)
    const [passwordInputHeight, setPasswordInputHeight] = useState(null)
    const [log, setLog] = useState({
 
        password: ''
    })

    // bCc * SiNCe1947

    const login = (data) => {
        props.closeModal(false)
    

    let body = {
            school_id: props.data.id,
            password: pass
        }
        ApiCall('/auth/school', body, "post", null, district).then(res => {
            
            if(res.status == 200){
                dispatch({
                    type: SET_LOADING,
                    data: false
                })
                dispatch({
                    type: SET_SUCCESS,
                    data: true
                })

                
                // navigation.navigate('HomeDrawer')
                props.passData(res.details.data.data)
                props.closeModal(false)


            }
            else{
                dispatch({
                    type: SET_SUCCESS,
                    data: true
                })
                dispatch({
                    type: SET_LOADING,
                    data: false
                })
                alert('Something went wrong.')

            }


        })
    }


    return (


        <Modal
            animationType="slide"
            transparent={true}
            presentationStyle="overFullScreen"
            visible={props.visible}
        >
            <KeyboardAvoidingView keyboardVerticalOffset={-100} enabled behavior={"padding"} style={{ flex: 1, alignItems: "center", backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: "center" }}>
                <View style={[{ backgroundColor: "#fff", width: width *.9, paddingHorizontal: 30, paddingVertical: 50, borderRadius: 8 }]}>

                    <View style={{ flexDirection: "row", alignItems: "center" }}>

                        <TouchableOpacity onPress={() => {
                            dispatch({
                                type:SCHOOL_LOGIN,
                                data: ''
                            })
                            props.close()
                        }}>

                            <ArrowLeftSVG
                                width={styles['icon-size'].width}
                                height={styles['icon-size'].height}
                                fill={styles['color-navy-blue'].color}
                            />

                        </TouchableOpacity>
                        <Text style={[styles['text-table-data'], { marginLeft: 12, color: styles['color-grey-blue'].color }]}>
                            Back to School selection
                        </Text>
                    </View>

                    <View style={{alignItems:"center", marginTop: 20}}>
                       
                        <Image resizeMode="contain" style={{ height: 126, width: 126 }} source={{uri: props.avatar}} />
                    </View>

                    <View style={{ flexDirection: "row",  alignItems: "center", marginTop: 16 }}>
                        <Image resizeMode="contain" style={{ height: 33, width: 33 }} source={props.imgSrc} />
                        <Text style={[styles['text-header'], { marginLeft: 8 }]}>{props.schoolName != "" && props.schoolName.name}</Text>
                    </View>

                    <View style={{ marginTop: 16 }}>
                

                        <View onLayout={(evt) => {

                            setPasswordInputHeight(evt.nativeEvent.layout.height)
                        }}>
                            <TextInput
                                secureTextEntry={showPass ? false : true}
                                value={pass}
                                onChangeText={(v) => {
                                    setLog({
                                        ...log,
                                        password: v
                                    })

                                    dispatch({
                                        type: SCHOOL_LOGIN,
                                        data: v
                                    })
                                }}
                                placeholder="Password"
                                placeholderTextColor="#c5c5c5"
                                style={[styles['text-input-style'], styles['with-border'], { width: "100%", marginTop: 24, color: "#000" }]}
                            />
                            <TouchableWithoutFeedback onPress={() => showPass ? setShowPass(false) : setShowPass(true)}>
                                <View style={{ position: "absolute", right: 10, top: passwordInputHeight != null ? passwordInputHeight / 2 : 10 }}>
                                    <ShowSVG />
                                </View>
                            </TouchableWithoutFeedback>
                        </View>


                    </View>



                    <View style={{ alignItems: "center" }}>
                        <ButtonComponent
                            onPress={login}
                            containerStyle={[, { backgroundColor: styles['color-blue'].backgroundColor, width: "100%", borderRadius: 8, marginTop: 40 }]}
                            text="Sign in" />

                        
                    </View>
                </View>

            </KeyboardAvoidingView>
        </Modal>

    )
}

export const TimeInModal = (props) => {


    
    const [currentTime, setCurrentTime] = useState(moment().format('HH:mm:ss A'))
    const [fadeAnim] = useState(new Animated.Value(0))


    if (props.show) {
        setTimeout(() => {
            Animated.timing(fadeAnim, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false
            }).start(({ finished }) => {

            })
        }, 450)
    }
    if (!props.show) {
        fadeAnim.setValue(0)
        fadeAnim.setOffset(0)
    }

    const backgroundInterpolation = fadeAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ["#00000000", "#00000090"]
    })


    setInterval(() => {
        setCurrentTime(moment().format('HH:mm:ss A'))
    })

    return(
        <Modal
            transparent={true}
            animationType={"slide"}
            visible={props.show}>
                
            <TouchableWithoutFeedback oonPress={props.exit}>
                <SafeAreaView style={{flex: 1}}>
                <Animated.View style={{flex: 1, marginBottom: -10,justifyContent:"flex-end", backgroundColor: backgroundInterpolation}}>
        
                        <View style={{ backgroundColor: "#fff", paddingBottom: 40 }}>

                            <View style={{ flexDirection: "row", padding: 30, justifyContent: "space-between" }}>
                                <View>
                                    <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color }]}>Attendance</Text>
                                    <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color, fontWeight: "bold", fontSize: 24, lineHeight: 32 }]}>{stored.user.selectedSubject.name}</Text>
                                </View>

                                <TouchableWithoutFeedback onPress={props.exit}>
                                    <CloseSVG height={24} width={24} fill="#000" />
                                </TouchableWithoutFeedback>


                            </View>

                            {
                                props.type != "confirm" && (
                                    <ImageBackground
                                        source={require('../assets/Vector_Map.png')}
                                        style={{ alignItems: "center", marginTop: 20, paddingVertical: 20 }}>
                                        <ClockSVG height={100} width={200} />
                                        <Text style={[styles['text-header'], { fontSize: 32, lineHeight: 43, fontWeight: "700", marginTop: 15 }]}>{currentTime}</Text>
                                        <Text style={[styles['text-header'], { color: styles['color-grey-blue'].color, fontSize: 20 }]}>{moment().format('dddd') + ", " + moment().format('MMMM DD')}</Text>

                                        <TouchableWithoutFeedback onPress={props.goLogs}>
                                            <Text style={[styles['text-table-data'], { marginTop: 25, fontSize: 18, color: styles['color-blue'].backgroundColor }]}>See logs</Text>
                                        </TouchableWithoutFeedback>
                                    </ImageBackground>
                                )
                            }


                         

                            <View style={{ alignItems: "center", alignSelf: "flex-end", width: "100%" }}>
                                <ButtonComponent
                                    onPress={() => props.confirm(currentTime)}
                                    containerStyle={[, { backgroundColor: styles['color-blue'].backgroundColor, width: "90%", borderRadius: 8, marginTop: 40 }]}
                                    text={props.type != "confirm" ? "Time " + (props.in ? "in" : "out") : "Yes, I am Timing " + (props.in ? "in" : "out")} />

                                <ButtonComponent
                                    onPress={props.cancel}
                                    containerStyle={[, { backgroundColor: "#CCD1D9", width: "90%", borderRadius: 8, marginTop: 20 }]}
                                    text={props.type != "confirm" ? "Cancel" : "Cancel Time "  + (props.in ? "in" : "out")} />
                            </View>


                        </View>
             
                </Animated.View>
                </SafeAreaView>
            </TouchableWithoutFeedback>

        </Modal>
    )
}



export const AlertModal = (props) => {
    const [currentTime, setCurrentTime] = useState(moment().format('HH:mm:ss A'))
    const [fadeAnim] = useState(new Animated.Value(0))


    if (props.show) {
        setTimeout(() => {
            Animated.timing(fadeAnim, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false
            }).start(({ finished }) => {

            })
        }, 450)
    }
    if (!props.show) {
        fadeAnim.setValue(0)
        fadeAnim.setOffset(0)
    }

    const backgroundInterpolation = fadeAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ["#00000000", "#00000090"]
    })


  

    return (
        <Modal
            transparent={true}
            animationType={"slide"}
            visible={props.show}>

            <TouchableWithoutFeedback oonPress={props.exit}>
             
                    <Animated.View style={{ flex: 1,  justifyContent: "center", alignItems:"center",backgroundColor: backgroundInterpolation }}>

                      
                                  <View style={{backgroundColor:"#fff", width:"80%", borderRadius: 8}}>
                                    <ImageBackground
                                        source={require('../assets/Vector_Map.png')}
                                        resizeMode="stretch"
                                        style={{ alignItems: "center", width: "100%", paddingVertical: 40 }}>
                                     
                                        
                                      <Text style={[styles['text-header'], { color: "#4FCD6A", fontSize: 20}]}>{"Time"+ (props.in ? " in " : " out " )+ "Successful!"}</Text>
                                      <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color, fontSize: 20, marginTop: 10 }]}>{props.time}</Text>
                                      
                                    </ImageBackground>
                                  </View>
                         




                    </Animated.View>
         
            </TouchableWithoutFeedback>

        </Modal>
    )
}


export const LoadingModal = (props) => {
    
    const [fadeAnim] = useState(new Animated.Value(0))


    if (props.show) {
        setTimeout(() => {
            Animated.timing(fadeAnim, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false
            }).start(({ finished }) => {

            })
        }, 400)
    }
    if (!props.show) {
        fadeAnim.setValue(0)
        fadeAnim.setOffset(0)
    }

    const backgroundInterpolation = fadeAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ["#00000000", "#00000090"]
    })




    return (
        <Modal
            transparent={true}
            animationType={"slide"}
            visible={props.show}>

            <TouchableWithoutFeedback oonPress={props.exit}>

                <Animated.View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: backgroundInterpolation }}>


                  
                  <ActivityIndicator
                    size="large"
                    color="#fff"
                  />




                </Animated.View>

            </TouchableWithoutFeedback>

        </Modal>
    )
}

export const SubjectConfirmationModal = (props) => {

    const [fadeAnim] = useState(new Animated.Value(0))


    if (props.show) {
        setTimeout(() => {
            Animated.timing(fadeAnim, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false
            }).start(({ finished }) => {

            })
        }, 400)
    }
    if (!props.show) {
        fadeAnim.setValue(0)
        fadeAnim.setOffset(0)
    }

    const backgroundInterpolation = fadeAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ["#00000000", "#00000090"]
    })


    

    return (
        <Modal
            style={{zIndex: 2}}
            transparent={true}
            animationType={"slide"}
            visible={props.show}>

            <TouchableWithoutFeedback oonPress={props.exit}>

                <Animated.View style={{ flex: 1, justifyContent: "flex-end", alignItems: "center", backgroundColor: backgroundInterpolation }}>



                <View style={{borderTopLeftRadius: 8, borderTopRightRadius: 8, width: width, backgroundColor:"#fff", paddingVertical: 50, paddingHorizontal: 20}}>

                    <Text style={[styles['text-table-data'], {color: styles['color-navy-blue'].color}]}>Change Subject</Text>

                    <View style={{marginTop: 28 }}>
                            {props.subject != undefined && <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color, fontSize: 24, lineHeight: 32 }]}>{props.subject.name}</Text>}
                    </View>

                {  props.subject != undefined &&  <View style={{alignItems:"center"}}>
                            <ButtonComponent
                                onPress={props.onChange}
                                containerStyle={[, { backgroundColor: styles['color-blue'].backgroundColor, width: "100%", borderRadius: 8, marginTop: 40 }]}
                                text={"Go to " + props.subject.name} />

                            <TouchableOpacity onPress={props.cancel} style={{padding: 20}}>
                                <Text style={[styles['text-table-data'], {color: styles['color-navy-blue'].color}]}>Cancel</Text>
                            </TouchableOpacity>
                    </View>}

                </View>




                </Animated.View>

            </TouchableWithoutFeedback>

        </Modal>
    )
}




const modalStyle = StyleSheet.create({
    borderTop: {
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    }
})