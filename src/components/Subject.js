import React, { useState, useEffeect } from 'react'
import { View, TouchableWithoutFeedback, Text, StyleSheet, TouchableOpacity } from "react-native";
import { styles } from '../stylesheet/style';
import { DeskSVG } from './IconSvg';
import  { useSelector} from 'react-redux'
export const SubjectItemDrawer = props => {
    const selectedSubject = useSelector(state => state.user.selectedSubject)

    console.log(selectedSubject, "--> WHUT")
    let item = props.item


    

    return (
        <View>
            <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>Subject</Text>

            <View style={{ marginTop: 20 }}>
                {item.map((i, index) => {
                    return (
                        <TouchableWithoutFeedback onPress={() => props.onPress(i)} key={index}>
                            <View style={[style.container, { height: 50, width: "100%" }, props.subjectContainerStyle, selectedSubject.id == i.id ? style.selectedSubject : style.unSelectedSubject]}>
                                <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color }]}>{i.name}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    )
                })}
            </View>
        </View>
    )
}

export const AllSubject = (props) => {

    return (
        <TouchableWithoutFeedback>
            <View style={[style.allSubject, style.alternateShadow]}>
                <DeskSVG height={24} width={24} fill="#fff" />
                <Text style={[styles['text-label'], { color: "#fff", fontWeight: "600", lineHeight: 19, marginLeft: 20 }]}>SEE ALL SUBJECTS</Text>
            </View>
        </TouchableWithoutFeedback>
    )

}


const style = StyleSheet.create({

    container: {
        borderTopLeftRadius: 8,
        borderBottomLeftRadius: 8,
        justifyContent: "center",
        paddingHorizontal: 20
    },
    alternateShadow:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: -5,
        },
        shadowOpacity: 0.3,
        shadowRadius: 10,

        elevation: 5,
    },
    allSubject: {
        backgroundColor: styles['color-blue'].backgroundColor,
        width: "100%",
        height: 60,
        alignItems: "center",
        paddingHorizontal: 20,
        flexDirection: 'row'
    },

    selectedSubject: {
        backgroundColor: 'rgba(74, 137, 232, 0.1)',
        borderRightWidth: 5,
        borderRightColor: styles['color-blue'].backgroundColor
    },
    unSelectedSubject: {
        backgroundColor: 'transparent',
        borderRightWidth: 0,
        borderRightColor: 'transparent'
    }
})
