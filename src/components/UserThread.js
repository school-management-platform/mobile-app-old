import moment from 'moment'
import React, {useState, useEffect, useRef} from 'react'
import {View, SafeAreaView, Text, TouchableOpacity, Image, Dimensions, TouchableWithoutFeedback, FlatList, Animated} from 'react-native'
import { styles } from '../stylesheet/style'
import {ThumbsSVG, OptionsSVG, ReplySVG, ThreadSVG} from './IconSvg'
const {width} = Dimensions.get('screen')
import Swipeable from 'react-native-gesture-handler/Swipeable';


export const UserThread = props => {
    let item = props.item
    const [showReply, setShowReplies] = useState(false)



    const rightButton = (progress, dragX) => {
        
        const trans = dragX.interpolate({
            inputRange: [-150, 1],
            outputRange: [-35, 150],
        });

        const transTwo = dragX.interpolate({
            inputRange: [-150, 1],
            outputRange: [-57, 150],
        });
        
        return(
            <TouchableOpacity>
                <Animated.View style={{ backgroundColor: "#7BC8FF", height: "100%", width: 123, alignItems: "center", justifyContent: "center", transform: [{ translateX: props.type == 1 ? trans : transTwo }] }} >
                    <ReplySVG width={24} height={24} fill="#fff" />
                    <Text style={[styles['text-header'], {color:"#fff", marginTop: 14, }]}>Reply</Text>
                   
                </Animated.View>
            </TouchableOpacity>
        )
    }

    

    return(
        <View key={props.index} style={{width: width, paddingHorizontal: 20}}>
            {
                props.type == 1 ? (
                    <Swipeable ref={(el) => props.swipeRef(props.type == 1 ? item.comment : item, el)} onSwipeableOpen={() => props.reply(props.type == 1 ? item.comment : item, props.type)} renderRightActions={rightButton} >
                        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image
                                    resizeMode="contain"
                                    source={{ uri: props.type == 1 ? item.comment.avatar : item.avatar }}
                                    style={{ height: 32, width: 32, borderRadius: 32 / 2, backgroundColor: "#dedede" }}
                                />

                                {
                                    props.type == 1 ? (
                                        <View style={{ marginLeft: 12 }}>
                                            <Text style={[styles['text-table-data'], { color: styles['color-navy-blue'].color }]}>{item.comment.firstname + " " + item.comment.middlename + " " + item.comment.lastname}</Text>
                                            <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>{moment(item.comment.updated_at).format('HH:mm A')}</Text>
                                        </View>
                                    )
                                        :
                                        (
                                            <View style={{ marginLeft: 12 }}>
                                                <Text style={[styles['text-table-data'], { color: styles['color-navy-blue'].color }]}>{item.firstname + " " + item.middlename + " " + item.lastname}</Text>
                                                <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>{moment(item.updated_at).format('HH:mm A')}</Text>
                                            </View>
                                        )
                                }
                            </View>


                        </View>

                        <View style={{ marginTop: 15 }}>

                            <Text style={[styles['text-mute'], { color: styles['color-navy-blue'].color, fontSize: 16 }]}>{props.type == 1 ? item.comment.message : item.message}</Text>

                        </View>
                    </Swipeable>
                )
                :
                (
                    <>
                            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                <View style={{ flexDirection: "row", alignItems: "center" }}>
                                    <Image
                                        source={{ uri: props.type == 1 ? item.comment.avatar : item.avatar }}
                                        style={{ height: 32, width: 32, borderRadius: 32 / 2, backgroundColor: "#dedede" }}
                                    />

                                    {
                                        props.type == 1 ? (
                                            <View style={{ marginLeft: 12 }}>
                                                <Text style={[styles['text-table-data'], { color: styles['color-navy-blue'].color }]}>{item.comment.firstname + " " + item.comment.middlename + " " + item.comment.lastname}</Text>
                                                <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>{moment(item.comment.updated_at).format('HH:mm A')}</Text>
                                            </View>
                                        )
                                            :
                                            (
                                                <View style={{ marginLeft: 12 }}>
                                                    <Text style={[styles['text-table-data'], { color: styles['color-navy-blue'].color }]}>{item.firstname + " " + item.middlename + " " + item.lastname}</Text>
                                                    <Text style={[styles['text-mute'], { color: styles['color-grey-blue'].color }]}>{moment(item.updated_at).format('HH:mm A')}</Text>
                                                </View>
                                            )
                                    }
                                </View>


                            </View>

                            <View style={{ marginTop: 15 }}>

                                <Text style={[styles['text-mute'], { color: styles['color-navy-blue'].color, fontSize: 16 }]}>{props.type == 1 ? item.comment.message : item.message}</Text>

                            </View>
                    </>
                )
                
            }

      

            {
                props.type == 1 && (
                    <View style={{ marginTop: 10}}>
                        {
                            item.replies.length >= 1 && (
                                <>
                                    <TouchableOpacity onPress={() => {
                                        setShowReplies(showReply ? false : true)
                                        props.reload()
                                    }}>
                                        <Text style={[styles['text-paragpraph'], { color: styles['color-blue'].backgroundColor }]}>{!showReply ? "View" : "Hide"} replies ({item.replies.length})</Text>
                                    </TouchableOpacity>
                                    {showReply && (
                                        <FlatList
                                            data={item.replies}
                                            showsVerticalScrollIndicator={false}
                                            contentContainerStyle={{ paddingBottom: 50 }}
                                            renderItem={({ item, index }) => {
                                                return (
                                                    <View style={{ marginTop: 24, flexDirection:"row" }}>
                                                        <ThreadSVG width={24} height={24} />
                                                        <View style={{marginLeft: -20}}>
                                                            <UserThread
                                                                reply={(v, type) => props.reply(v, type)}
                                                                swipeRef={(val, el) => props.swipeRef(val, el)}
                                                                type={2}
                                                                index={index}
                                                                item={item}
                                                            />
                                                        </View>
                                                    </View>
                                                )
                                            }}
                                        />
                                    )}
                                </>


                            )
                        }



                    </View>
                )
            }
        </View>
    )
}