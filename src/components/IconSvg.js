import Logo from "../assets/svg/Logo.svg";
import ArrowLeft from "../assets/svg/Icon_Arrow.svg";
import Show from '../assets/svg/Show.svg'
import Chat from '../assets/svg/Icon_Chat.svg'
import Home from '../assets/svg/Icon_Home.svg'
import Upload from '../assets/svg/Icon_Upload.svg'
import VirtualClass from '../assets/svg/Icon_VirtualClass.svg'
import Time from '../assets/svg/Icon_Time.svg'
import Menu from '../assets/svg/menu.svg'
import Lock from '../assets/svg/Icon_Lock.svg'
import Announcement from '../assets/svg/Icon_Announcement.svg'
import Student from '../assets/svg/Icon_Student2.svg'
import Parents from '../assets/svg/Icon_Parents.svg'
import Settings from '../assets/svg/Settings.svg'
import Search from '../assets/svg/Icon_Search.svg'
import Desk from '../assets/svg/Icon_Desk.svg'
import CheckMark from '../assets/svg/Saly-26.svg'
import Calendar from '../assets/svg/Icon_Calendar.svg'
import Close from '../assets/svg/close.svg'
import Clock from '../assets/svg/Vector_Clock.svg'
import FileIcon from '../assets/svg/Icon_File.svg'
import Submitted from '../assets/svg/Icon_Submitted.svg'
import NotSubmitted from '../assets/svg/Icon_Not_Submitted.svg'
import Attach from '../assets/svg/attach_file.svg'
import Thumbs from '../assets/svg/Thumbs.svg'
import Options from '../assets/svg/Options.svg'
import Send from '../assets/svg/Icon_Send.svg'
import Reply from '../assets/svg/Icon_Reply.svg'
import Thread from '../assets/svg/Thread.svg'
import Check from '../assets/svg/Icon_Check.svg'

export const LogoSVG = Logo
export const ArrowLeftSVG = ArrowLeft
export const ShowSVG = Show
export const ChatSVG = Chat
export const HomeSVG = Home
export const UploadSVG = Upload
export const VirtualSVG = VirtualClass
export const TimeSVG = Time
export const MenuSVG = Menu
export const LockSVG = Lock
export const AnnouncementSVG = Announcement
export const StudentSVG = Student
export const ParentsSVG = Parents
export const SettingsSVG = Settings
export const SearchSVG = Search
export const DeskSVG = Desk
export const CheckMarkSVG = CheckMark
export const CalendarSVG = Calendar
export const CloseSVG = Close
export const ClockSVG = Clock
export const FileSVG = FileIcon
export const SubmittedSVG = Submitted
export const AttachSVG = Attach
export const NotSubmittedSVG = NotSubmitted
export const ThumbsSVG = Thumbs
export const OptionsSVG = Options
export const SendSVG = Send
export const ReplySVG =Reply
export const ThreadSVG = Thread
export const CheckSVG = Check
