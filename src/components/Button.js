import React from "react";
import { TouchableWithoutFeedback, View, Text, StyleSheet, Dimensions } from "react-native";
import { styles } from "../stylesheet/style";
const {width} = Dimensions.get('screen')

export const ButtonComponent = (props) => {

    return(
        <TouchableWithoutFeedback onPress={props.onPress} {...props}>
            <View style={[buttonStyle.container, props.containerStyle]}>
                <Text style={[styles["text-header"],{color: styles["color-white"].color}, props.textColor]}>{props.text}</Text>
            </View>
        </TouchableWithoutFeedback>
    )

}

const buttonStyle = StyleSheet.create({
    container:{
        paddingVertical: 14,
        width: width  * .8,
        alignItems:"center",
        justifyContent:"center",
        borderRadius: 8
    }
})