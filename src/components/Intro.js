import React from 'react'
import { View, Text } from 'react-native'
import { useSelector } from 'react-redux'
import { styles } from '../stylesheet/style'

export const Intro = (props) => {
    const userData = useSelector(state => state.user.details.user)
    const subject = useSelector(state => state.user.selectedSubject)

    
    return (
        <View style={[{ paddingHorizontal: 20 }, props.containerStyle]}>
            { subject != null && (
                <>
                    <Text style={[styles['text-paragpraph'], { fontWeight: "600", fontSize: 18, lineHeight: 24 }]}>{userData.firstname} {userData.middlename} {userData.lastname}</Text>
                    <Text style={[styles['text-header'], { lineHeight: 38, fontSize: 28, fontWeight: "700" }]}>{subject != null && subject.name}</Text>
                    <Text style={[styles['text-header'], { fontSize: 14, lineHeight: 19 }]}>We have few things for you to look</Text>
                </>
            )
}
        </View>
    )
}


