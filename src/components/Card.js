import { useNavigation } from '@react-navigation/native'
import moment from 'moment'
import React, { useState } from 'react'
import { View, Text, TouchableWithoutFeedback, Image, StyleSheet, Dimensions, Linking, Alert, Platform, PermissionsAndroid } from 'react-native'
import { styles } from '../stylesheet/style'
import { baseUrl, baseUrlSchoolLogo, fileUrl } from '../utils/api'
import { ButtonComponent } from './Button'
import { CalendarSVG, CheckMarkSVG, ClockSVG, LockSVG, TimeSVG, FileSVG, SubmittedSVG, AttachSVG, NotSubmittedSVG, CheckSVG } from './IconSvg'
import RNFetchBlob from 'rn-fetch-blob';
const { fs, config } = RNFetchBlob
import FastImage from 'react-native-fast-image'
const {width, height} = Dimensions.get('screen')
const weeks = ["Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
export const Card = (props) => {
    let item = props.item
    
    // 
    return (
        <TouchableWithoutFeedback onPress={props.cardPress} key={props.index} >
            <View style={[cardStyle.box, cardStyle.marginLeft]}>
                {/* <Image
                    source={{uri: baseUrlSchoolLogo +"/"+ item.logo}}
                    resizeMode="contain"
                    style={{ width: 145, height: 149 }}
                /> */}
                <FastImage
                    style={{ width: 145, height: 140 }}
                    source={{
                        uri: baseUrlSchoolLogo + "/" + item.logo,
                        headers: { Authorization: 'someAuthToken' },
                        priority: FastImage.priority.high,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <Text style={[styles['text-header'], { marginTop: 10 }]}>{item.name}</Text>
                <Text style={[styles['text-muted'], { fontWeight: "normal", color: "#8E93A5" }]}>{item.address}</Text>
            </View>
        </TouchableWithoutFeedback>
    )
}

export const SubjectCard = (props) => {
    let item = props.item

    
    

 
    return (
        <TouchableWithoutFeedback onPress={async() => {
          
            const supported = await Linking.canOpenURL(item.link);
            
            if (supported) {
                // alert('pumasok ba to')
                // Opening the link with some app, if the URL scheme is "http" the web link should be opened
                // by some browser in the mobile
                await Linking.openURL(item.link);
            } else {
                Alert.alert(`Don't know how to open this URL: ${item.link}`);
            }

        }} key={props.index} >
            <View style={[{ backgroundColor:styles['color-peach'].color, marginBottom: 10, flexShrink: 1 }, cardStyle.subjectBox, props.containerStyle, cardStyle.shadow]}>
                <Text style={[styles['text-header'], { marginTop: 20, color: props.identifier != "past" && props.identifier != "upcoming" ? styles['color-brown'].color : "#000"}]}>{item.name}</Text>
                <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20, }}>
                    <View style={{ opacity: props.identifier == "upcoming" ? 0.5 : 1 }}>
                    <LockSVG width={13} height={17.5} fill={props.identifier != "past" && props.identifier != "upcoming" ? styles['color-brown'].color : "#000"} />
                    </View>
                    <Text style={[styles['text-table-data'], { color: props.identifier != "past" && props.identifier != "upcoming" ? styles['color-brown'].color : "#000", marginLeft: 13 }]}>{item.password}</Text>
                    
                </View>
                <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20, }}>
                    <View style={{ opacity: props.identifier == "upcoming" ? 0.5 : 1 }}>
                        <TimeSVG width={13} height={17.5} fill={props.identifier != "past" && props.identifier != "upcoming" ? styles['color-brown'].color : "#000"} />
                    </View>
                    <Text style={[styles['text-table-data'], { color: props.identifier != "past" && props.identifier != "upcoming" ? styles['color-brown'].color : "#000", marginLeft: 13 }]}>{moment(item.updated_at).format('HH:mma')}</Text>

                </View>

                {
                    props.type == "virtual"  && (
                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20, }}>
                            <View style={{ opacity: props.identifier == "upcoming" ? 0.5 : 1}}>
                                <CalendarSVG width={13} height={17.5} fill={props.identifier != "past" && props.identifier != "upcoming" ? styles['color-brown'].color : "#000"} />
                            </View>
                            <Text style={[styles['text-table-data'], { color: props.identifier != "past" && props.identifier != "upcoming" ? styles['color-brown'].color : "#000", marginLeft: 13 }]}>{"2021-05-06"}</Text>
                        </View>
                    )
                }
                {/* <View style={{ flexDirection: "row", alignItems: "center", marginTop: 11 }}>
                    <TimeSVG width={13} height={17.5} fill={styles['color-brown'].color} />
                    <Text style={[styles['text-table-data'], { color: styles['color-brown'].color, marginLeft: 13 }]}>{"4:00pm"}</Text>
                </View> */}

               
                    {
                    props.identifier != "past" && props.identifier != "upcoming" && (
                        <Image
                            resizeMode="stretch"
                            style={{ height: 140, width: 130, alignSelf: "flex-end" }}
                            source={require('../assets/Saly-24.png')}
                        />
                        )
                    }
               

                {/* <ButtonComponent
                    textColor={{ color: styles['color-dark-blue'].color }}
                    containerStyle={{ backgroundColor: styles['color-white'].backgroundColor, width: "100%", marginTop: 49 }}
                    text="Join"
                /> */}
            </View>
        </TouchableWithoutFeedback>
    )
}

export const AnnouncementCard = (props, navigation) => {
    
    let item = props.item

    
    
    const navigate = () => {

        
        
            navigation.navigate('Announcement', {
                data: {
                    id: item.key,
                    message: item.value
                }
            })
    }

    return (
        <View style={[{  marginLeft: 20 }]}>
            <TouchableWithoutFeedback onPress={navigate} key={props.index} >
           
                    <View style={cardStyle.shadow}>
                    <View style={{position:"absolute", top: 20, left: 20, zIndex: 2}}>
                        {item.icon}
                    </View>
                    <Image
                        // resizeMode="contain"
                        style={[{ width: width / 1.3, height: 170, }, cardStyle.borderRadius]}
                        source={item.img} />
                    </View>
           
            </TouchableWithoutFeedback>

            <View style={{marginTop: 10}}>
                <Text style={[styles['text-header'], {lineHeight: 27, fontSize: 20}]}>{item.name}</Text>
                <Text style={[styles['text-mute'], {lineHeight: 19}]}>{item.date}</Text>
            </View>
        </View>
    )

}

export const ScheduleCard = (props, length) => {
    let item = props.item
    let index = props.index
    var today = new Date()

    

    let todayWeek = weeks[today.getDay()] == item.day
    
    
    
    return(
        <TouchableWithoutFeedback>
            <View style={[{ marginLeft: 20, marginRight: (index + 1) == length ? 20: 0 ,width: width / 1.3,   padding: 20, marginBottom: 5},todayWeek ? cardStyle.currentWeek : cardStyle.notCurrentWeek, cardStyle.borderRadius, cardStyle.shadow]}>

                <View style={{flexDirection:"row", width:"100%", justifyContent:'space-between', alignItems:"center"}}>
                    <Text style={[styles['text-label'], todayWeek ? cardStyle.currentWeekText : cardStyle.notCurrentWeekText, { fontSize: 24, lineHeight: 32 }]}>{item.day}</Text>
                    <CheckMarkSVG height={24} width={24}/>
                </View>

                <View style={{ marginTop: 44 }}>
                    <View style={[{ flexDirection: "row", width: "100%", justifyContent: "space-between" }]}>
                        <Text style={[styles['text-table-data'], { color: styles['color-navy-blue'].color }]}>Morning</Text>
                        {/* <View style={[{ flexDirection: "row", alignItems: "center" }]}>
                            <TimeSVG fill={todayWeek ? "#832FC570" : "#CCD1D9" } width={24} height={24} />
                            <Text style={[styles['text-table-data'], { color: styles['color-navy-blue'].color, marginLeft: 17 }]}>{item.morning.time}</Text>
                        </View> */}
                    </View>
                </View>

                <View style={{marginTop: 16, width:"100%"}}>
                    <Text style={[styles['text-mute'], todayWeek ? cardStyle.currentWeekText : cardStyle.notCurrentWeekText]}>Mode of Learning</Text>
                        <Text style={[styles['text-header'], {color: styles['color-navy-blue'].color}]}>{item.mol_desc_1}</Text>
                </View>

                <View style={{ marginTop: 24, height: 1, width: "90%", borderWidth: 0.5, borderColor:"#CCD1D9"}}/>


                <View style={{ marginTop: 24 }}>
                    <View style={[{ flexDirection: "row", width: "100%", justifyContent: "space-between" }]}>
                        <Text style={[styles['text-table-data'], { color: styles['color-navy-blue'].color }]}>Afternoon</Text>
                        {/* <View style={[{ flexDirection: "row", alignItems:"center" }]}>
                            <TimeSVG fill={todayWeek ? "#832FC570" : "#CCD1D9"} width={24} height={24} />
                            <Text style={[styles['text-table-data'], { color: styles['color-navy-blue'].color, marginLeft: 17 }]}>{item.afternoon.time}</Text>
                        </View> */}
                    </View>
                </View>

                <View style={{ marginTop: 16, width: "100%" }}>
                    <Text style={[styles['text-mute'], todayWeek ? cardStyle.currentWeekText : cardStyle.notCurrentWeekText]}>Mode of Learning</Text>
                    <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color }]}>{item.mol_desc_2}</Text>
                </View>




            </View>
        </TouchableWithoutFeedback>
    )
}

export const CardQuote = (props) => {
    
    return(
        <View style={[{ width: width * .9, alignSelf: "center", backgroundColor: "#8E9DD3", padding: 20, paddingBottom: 0}, cardStyle.borderRadius, cardStyle.shadow]}>
            <Text style={[styles['text-label'], {color:"#fff", lineHeight: 19, letterSpacing:1.25}]}>QUOTE OF THE DAY</Text>

            <View style={{flexDirection:"row", justifyContent:"space-between", marginTop: 13, height: 25}}>
                <Text style={[styles['text-header'], {fontSize: 32, lineHeight: 43, color:"#fff"}]}>“</Text>
                <Text style={[styles['text-header'], { fontSize: 32, lineHeight: 43, color: "#fff" }]}>”</Text>
            </View>

            <Text style={[styles['text-mute'], {color:"#fff"}]}>{props.text != undefined  &&props.text.value} </Text>

            {/* <Text style={[styles['text-table-data'], {color:"#fff", marginTop: 20, lineHeight: 19, fontWeight:"600",}]}> - Jods, 3000</Text> */}
            
            
                <Image
                    
                    source={require('../assets/Saly-17.png')}
                />
            
        </View>
    )
}

export const SeatworkCard = (props) => {
    const navigation = useNavigation()
    let item = props.item
    let dateToRender
    let timeToRender
    const [file, setFile] = useState(null)
    
    let finalUrl = baseUrl + "/" + item.link

    if(props.type == 1){
        finalUrl = baseUrlSchoolLogo + "/" + item.link
        dateToRender = moment(item.updated_at).format("MMMM DD, YYYY")
        timeToRender = moment(item.updated_at).format('HH:mmA')
    }
    else if(props.type == 4){
        finalUrl =  item.file
        dateToRender = item.deadline_date
        timeToRender = moment("2020/05/05 "+item.deadline_time).format('hh:mmA')
    }
    else{
      finalUrl =  fileUrl + "/" + item.link
    dateToRender = moment(item.updated_at).format("MMMM DD, YYYY")
        timeToRender = moment(item.updated_at).format('HH:mmA')
    }


    const downloadPdf = () => {

        if (Platform.OS === 'ios') {
       
            downloadFIle()
        } else {
            try {
                PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'Storage permission',
                        message: 'Asking for storage permission to store the pdf on your device.',
                    },
                ).then(granted => {
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //Once user grant the permission start downloading
                     
                        downloadFIle()
                    } else {
                        //If permission denied then show alert 'Storage Permission 

                    }
                });
            } catch (err) {
                //To handle permission related issue

            }
        }
    }

    const downloadFIle = () => {
        var pdf_url = finalUrl
        let PictureDir = Platform.OS == "ios" ? fs.dirs.DocumentDir : fs.dirs.DownloadDir

        let options = {
            fileCache: true,
            addAndroidDownloads: {
                //Related to the Android only
                useDownloadManager: true,
                notification: true,
                path:
                    PictureDir + `/${item.name}.pdf`,
                description: 'History File',
            },
            path: PictureDir + `/${item.name}.pdf`,
            appendExit: 'pdf'
        };

        config(options)
            .fetch('GET', pdf_url)
            .then(res => {
                //Showing alert after successful downloading
                // 


                // Alert.alert('Download succesfully');
                if (Platform.OS === "ios") {

                    
                    setTimeout(() => {
                        RNFetchBlob.ios.previewDocument(res.data);
                    }, 500)
                }
                else {

                    Alert.alert("File", "Download succesfully.", [
                        {
                            text: "Ok",
                        }
                    ])
                }
                // RNFetchBlob.ios.openDocument(res.data);
            });
    }

    
    

    

    return(
        <TouchableWithoutFeedback onPress={props.onPress}>
            <View style={[{ width: width * .9, borderRadius: 8, padding: 20, backgroundColor: "#fff" }, cardStyle.shadow]}>

                <Text style={[styles['text-header'], { color: styles['color-navy-blue'].color, fontSize: 20, lineHeight: 27 }]}>{item.name || item.title}</Text>

                {props.type == 4 && (
                    <View style={{marginTop: 20}}>
                        <Text style={[styles['text-paragpraph'], {color:styles['color-grey-blue'].color}]}>{item.description}</Text>
                    </View>
                )}

                <View style={{ flexDirection: "row", alignItems: "center", marginTop: 25 }}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <CalendarSVG height={16} width={16} fill={props.type == 4 && item.submitted_at == null ? "red" : styles['color-navy-blue'].color} />
                        <Text style={[styles['text-label'], { color: props.type == 4 && item.submitted_at == null ? "red"  : styles['color-navy-blue'].color, marginLeft: 10 }]}>{dateToRender}</Text>
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center", marginLeft: 31 }}>
                        <TimeSVG height={16} width={16} fill={props.type == 4 && item.submitted_at == null ? "red" : styles['color-navy-blue'].color} />
                        
                        <Text style={[styles['text-label'], { color: props.type == 4 && item.submitted_at == null ? "red" : styles['color-navy-blue'].color, marginLeft: 10 }]}>{timeToRender}</Text>
                    </View>


                </View>
                {console.log(props.item)}

                {props.type == 4 && (
                    <View style={{marginTop: 17}}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <FileSVG height={16} width={16} fill={styles['color-navy-blue'].color} />
                            <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>PDF</Text>
                        </View>
                        
                        {
                            item.status == 1 ?  (
                                <>

                                  
                                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 12 }}>
                                        <SubmittedSVG height={16} width={16} fill="#5AE579" />

                                        <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{item.submitted_at}</Text>
                                        {/* <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{moment(item.updated_at).format("MMMM DD, YYYY, HH:mmA")}</Text> */}
                                    </View>
                                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 12 }}>
                                        <CheckSVG height={16} width={16} fill="#5AE579" />

                                        <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{item.score + "/" + item.max_score}</Text>
                                        {/* <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{moment(item.updated_at).format("MMMM DD, YYYY, HH:mmA")}</Text> */}
                                    </View>
                  

                                </>
                            )
                            :
                            (
                                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 12 }}>
                                        <NotSubmittedSVG height={16} width={16} fill="#E84A5F" />
                                        <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>Not yet submitted</Text>
                                        {/* <Text style={[styles['text-label'], { color: styles['color-navy-blue'].color, marginLeft: 10 }]}>{moment(item.updated_at).format("MMMM DD, YYYY, HH:mmA")}</Text> */}
                                    </View>
                            )
                        }
                        
                    </View>
                )}

                <View style={{flexDirection:"row", alignItems:"center", marginTop: 17}}>
                    {props.type == 4 && finalUrl != null && (
                        <AttachSVG height={16} width={16} fill="#5AE579" />
                    )}
                    
                    {
                        finalUrl != null && (
                            <TouchableWithoutFeedback onPress={async () => {
                                const supported = await Linking.canOpenURL(baseUrlSchoolLogo + "/" + item.link);

                             

                                if(props.type == 2 || props.type == 3 || props.type == 4){
                                 
                                    downloadPdf()
                                }
                                else{
                                    navigation.navigate('Link', {
                                        link: finalUrl
                                    })
                                }
                            }}>

                                <Text style={[styles['text-mute'], { color: styles['color-blue'].backgroundColor, textDecorationLine: "underline", marginLeft: props.type == 4 ? 12 : 0 }]}>Link here</Text>
                            </TouchableWithoutFeedback>
                        )
                    }
                </View>

            </View>
        </TouchableWithoutFeedback>
    )

}

const cardStyle = StyleSheet.create({
    box: {
        height: 225,
        width: 225,
        backgroundColor: styles['color-white'].backgroundColor,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center"
    },
    currentWeek:{
        backgroundColor:"#EEE7FF",
      
    },
    currentWeekText:{
        color:"#832FC5" ,
       
    },
    notCurrentWeekText: {
        color: "#8E93A5",
        
        
    },
    notCurrentWeek:{
        backgroundColor:"#FFF",
        opacity: 0.5,
    } , 
    marginLeft: {
        marginLeft: 20
    },
    borderRadius:{
        borderRadius: 8
    },  
    subjectBox: {
        // width: 200,
        // height: 222,,
        paddingHorizontal: 20,
        marginLeft: 20,
        borderRadius: 8
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 5,

        elevation: 5,
        marginTop: 10
    }
})

