import React from 'react'
import { View, TouchableWithoutFeedback, Text, Image, StyleSheet, SafeAreaView, TextInput, Animated } from "react-native";
import { store } from '../Redux/confiigureStore';
import { styles } from '../stylesheet/style';
import { AnnouncementSVG, ArrowLeftSVG, MenuSVG, ParentsSVG, SearchSVG, SettingsSVG, StudentSVG } from "./IconSvg";
export const Header = (props) => {
    
    let stored = store.getState()
    let userData = stored.user.details

    let leftIconRender
    let rightIconRender

    if (props.type == 1) {
        leftIconRender = <TouchableWithoutFeedback disabled={props.disablePress} onPress={props.onBack}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
                <ArrowLeftSVG width={24} heigth={24} fill={"#000"} />
                <Text style={[styles['text-table-data'], { color: styles['color-navy-blue'].color, marginLeft: 12 }]}>Back</Text>
            </View>
        </TouchableWithoutFeedback>
        
        rightIconRender = props.rightIcon

        if(props.rightIcon.id == "announcement"){
            rightIconRender = <AnnouncementSVG width={27} height={30} fill="#fff" />
        }
        else if(props.rightIcon.id == "student_manual"){
            rightIconRender = <StudentSVG width={27} height={30} fill="#fff"/>
        }
        else if (props.rightIcon.id == "parent_manual") {
            rightIconRender = <ParentsSVG width={27} height={30} fill="#fff" />
        }
    }
    else {
        leftIconRender =
            <TouchableWithoutFeedback onPress={props.onToggle}>
                <View style={{}}>
                    <MenuSVG width={24} heigth={16} fill={styles['color-navy-blue'].color} />
                </View>
            </TouchableWithoutFeedback>

        rightIconRender = <View style={style.shadow}>
            <Image
                resizeMode="contain"
                source={{uri: userData.student.avatar}}
                style={{ height: 40, width: 40, borderRadius: 20, backgroundColor: "#c5c5c5" }}
            />
        </View>
    }
    


    return (
        <Animated.View style={[{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingHorizontal: 20, marginTop: 26, backgroundColor:"#fff" }, props.containerStyle]}>

            {leftIconRender}

            {rightIconRender}
          

        </Animated.View>
    )

}

export const DrawerHeader = (props) => {
    let user = props.data.student
    let records = props.data.record[0]

    
//  No avatar yet
    return (
        <View style={[{ backgroundColor: styles['color-blue'].backgroundColor, width: "100%", paddingLeft: 24, marginBottom: 46 }, style.headerDrawerShadow]}>
            <SafeAreaView>
                <View style={{ flexDirection: 'row', marginTop: 44, width: "100%", justifyContent: "space-between" }}>
                    <Image
                            resizeMode="contain"
                            source={{uri: user.avatar}}
                            style={[style.avatar, {}]} />

                    {/* <View style={{ marginRight: 21 }}>
                        <SettingsSVG height={24} width={24} fill={"#fff"} />
                    </View> */}
                </View>

                <View style={{ marginTop: 15 }}>
                    <Text style={[styles['text-header'], { lineHeight: 24, fontSize: 18, fontWeight: "700", color: "#fff" }]}>{user.firstname +" " +user.middlename + " " + user.lastname}</Text>
                    <Text style={[styles['text-mute'], { lineHeight: 19, color: "#fff", opacity: 0.7 }]}>{records.level_name}: {records.section_name}</Text>
                </View>

                <View style={{ marginTop: 30 }}>
                    <View style={{ position: "absolute", zIndex: 2, top: 8, left: 19 }}>
                        <SearchSVG fill={"#A4ADBB"} />
                    </View>
                    <TextInput
                        onChangeText={(val) => props.onChangeText(val)}
                        style={[style.input, styles['text-paragpraph'], {color:"#000"}]}
                        placeholder="Search Subject..."
                    />
                </View>
            </SafeAreaView>
        </View>
    )

}

const style = StyleSheet.create({
    avatar: {
        backgroundColor: "#c5c5c5",
        height: 60,
        width: 60,
        borderRadius: 30
    },
    headerDrawerShadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 14,
        },
        shadowOpacity: 0.15,
        shadowRadius: 5,

        elevation: 5,
    },
    input: {
        backgroundColor: "#fff",
        borderRadius: 8,
        height: 37,
        width: "90%",
        paddingVertical: 8,
        paddingLeft: 56,
        marginBottom: 37,

    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }
})