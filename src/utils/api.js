import axios from 'axios'

// export const baseUrl = 'http://172.104.168.24:8000'

// export const baseUrlSchoolLogo = 'http://172.104.168.24:8001'
// export const fileUrl = 'https://doa-s3.s3-ap-southeast-1.amazonaws.com'


// PROD

export const baseUrl = 'http://api.dioceseofantipolo.net'

export const baseUrlSchoolLogo = 'https://dioceseofantipolo.net/s/public'

export const baseurl2 = 'http://api.mapsaantipolo.net'
export const baseUrl2SchoolLogo = 'http://api.mapsaantipolo.net/s/public'

export const fileUrl = 'https://doa-s3.s3-ap-southeast-1.amazonaws.com'


export const ApiCall = (url, body, method, token, district) => {


    console.log((district == 1 ? baseUrl : baseurl2) + url, "--> TANGA", body)

    return axios({
        method: method,
        url: (district == 1 ? baseUrl : baseurl2) + url,
        params: body,
        data: null,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
    })
        .then((res) => {
            
            return {
                error: false,
                details: res,
                status: 200
            };
        })
        .catch((error) => {

                
                
            return {
                error: true,
                details: error,
                status: error.response.status
            };
        });
};

export const ApiCallFormData = (url, body, method, token, district) => {


    let config = {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${token}`,
        },
    };

    return axios.post((district == 1 ? baseUrl : baseurl2) + url, body, config).then(res => {

        return {
            error: false,
            details: res,
            status: 200
        }
    }).catch((error) => {



        return {
            error: true,
            details: error,
            status: error.response.status
        }
    })
}
