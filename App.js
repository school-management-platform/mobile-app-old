import { NavigationContainer } from '@react-navigation/native'
import React from 'react'

import { AuthStack } from './src/navigator/Stack/AuthStack'
import { Provider } from 'react-redux'
import { store } from './src/Redux/confiigureStore'
const App = () => {
  return (
    <NavigationContainer>
     <Provider store={store}>
        <AuthStack />
     </Provider>
    </NavigationContainer>
  )
}

export default App
